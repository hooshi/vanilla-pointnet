/* Copyright (C) 2001-2017 Peter Selinger.
   This file is part of Potrace. It is free software and it is covered
   by the GNU General Public License. See the file COPYING for details. */

/* A simple and self-contained demo of the potracelib API */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <assert.h>

#include "../potracelib.h"


// ====================================================================== 
// auxiliary bitmap functions so that we can talk to potrace
// ====================================================================== 

/* macros for writing individual bitmap pixels */
#define BM_WORDSIZE ((int)sizeof(potrace_word))
#define BM_WORDBITS (8*BM_WORDSIZE)
#define BM_HIBIT (((potrace_word)1)<<(BM_WORDBITS-1))
#define bm_scanline(bm, y) ((bm)->map + (y)*(bm)->dy)
#define bm_index(bm, x, y) (&bm_scanline(bm, y)[(x)/BM_WORDBITS])
#define bm_mask(x) (BM_HIBIT >> ((x) & (BM_WORDBITS-1)))
#define bm_range(x, a) ((int)(x) >= 0 && (int)(x) < (a))
#define bm_safe(bm, x, y) (bm_range(x, (bm)->w) && bm_range(y, (bm)->h))
#define BM_USET(bm, x, y) (*bm_index(bm, x, y) |= bm_mask(x))
#define BM_UCLR(bm, x, y) (*bm_index(bm, x, y) &= ~bm_mask(x))
#define BM_UPUT(bm, x, y, b) ((b) ? BM_USET(bm, x, y) : BM_UCLR(bm, x, y))
#define BM_PUT(bm, x, y, b) (bm_safe(bm, x, y) ? BM_UPUT(bm, x, y, b) : 0)

/* return new un-initialized bitmap. NULL with errno on error */
static potrace_bitmap_t *bm_new(int w, int h) {
  potrace_bitmap_t *bm;
  int dy = (w + BM_WORDBITS - 1) / BM_WORDBITS;

  bm = (potrace_bitmap_t *) malloc(sizeof(potrace_bitmap_t));
  if (!bm) {
    return NULL;
  }
  bm->w = w;
  bm->h = h;
  bm->dy = dy;
  bm->map = (potrace_word *) calloc(h, dy * BM_WORDSIZE);
  if (!bm->map) {
    free(bm);
    return NULL;
  }
  return bm;
}

/* free a bitmap */
static void bm_free(potrace_bitmap_t *bm) {
  if (bm != NULL) {
    free(bm->map);
  }
  free(bm);
}

// ====================================================================== 
// the core tracing functions 
// ======================================================================

// ===============================
struct polyline_list_s {
  int n_segment;
  int *sign;
  int *xadj;
  double *points;
};


typedef struct polyline_list_s polyline_list_t;

static polyline_list_t
polyline_list_create(){
  polyline_list_t point_list;
  point_list.n_segment = 0;  
  point_list.sign = NULL;
  point_list.xadj = NULL;
  point_list.points = NULL;
  return point_list;
}

static void
polyline_list_destroy(polyline_list_t poly_list){
  if(poly_list.sign)    free(poly_list.sign);
  if(poly_list.points)  free(poly_list.points);
  if(poly_list.xadj)    free(poly_list.xadj);
}
  
// ===============================
static double
bernstein_value_at(double t, double const * c_, unsigned n){
  double u = 1.0 - t;
  double bc = 1;
  double tn = 1;
  double tmp = c_[0] * u;
  for(unsigned i = 1; i < n; i++){
    tn = tn * t;
    bc = bc * (n - i + 1) / i;
    tmp = (tmp + tn * bc * c_[i]) * u;
  }
  return (tmp + tn * t * c_[n]);
}

static potrace_dpoint_t
bezier_value_at(potrace_dpoint_t *c, const double t)
{
  potrace_dpoint_t ans;

  // c must have a size of 4!
  assert(t <= 1.);
  assert(t >= 0);
  
  const int order = 3;
  double control_x_vals[] = {c[0].x, c[1].x, c[2].x, c[3].x};
  double control_y_vals[] = {c[0].y, c[1].y, c[2].y, c[3].y};

  ans.x = bernstein_value_at(t, control_x_vals, order);
  ans.y = bernstein_value_at(t, control_y_vals, order);

  return ans;
}

// vals[width*j + i] is the pixel on the jth column and ith row
static polyline_list_t
trace(int width, int height, int *pixel_vals, potrace_param_t *param) {
  const int n_points_per_bezier = 50;
  int x, y, i, j, isegment, ipoint;
  potrace_bitmap_t *bm;
  potrace_path_t *p;
  potrace_state_t *st;
  int n, *tag;
  potrace_dpoint_t (*c)[3];
  int bit_value;
  polyline_list_t out = polyline_list_create();
  potrace_dpoint_t ctrl_pts[4];
    
  /* create a bitmap */
  bm = bm_new(width, height);
  if (!bm) {
    fprintf(stderr, "Error allocating bitmap: %s\n", strerror(errno)); 
    return polyline_list_create();
  }

  /* fill the bitmap with some pattern */
  for (y=0; y<height; y++) {
    for (x=0; x<width; x++) {
      bit_value =  pixel_vals[y*width + x] > 0;
      BM_PUT(bm, x, y, bit_value);
    }
  }

  /* set tracing parameters, starting from defaults */
  // param->turdsize = 0;

  /* trace the bitmap */
  st = potrace_trace(param, bm);
  if (!st || st->status != POTRACE_STATUS_OK) {
    fprintf(stderr, "Error tracing bitmap: %s\n", strerror(errno));
    return polyline_list_create();
  }
  bm_free(bm);

  // Count number of segments
  out.n_segment = 0;
  p = st->plist;
  while (p != NULL) {
    ++out.n_segment;
    p = p->next;
  }

  // Count number of points to output
  out.xadj = calloc(out.n_segment+1, sizeof(int));
  out.sign = calloc(out.n_segment, sizeof(int));
  out.xadj[0] = 0;

  p = st->plist;
  isegment = 0;
  while (p != NULL) {
    n = p->curve.n;
    tag = p->curve.tag;
    c = p->curve.c;
    out.xadj[isegment+1] = out.xadj[isegment]; 
    out.sign[isegment] = p->sign;
    for (i=0; i<n; i++) {
      switch (tag[i]) {
      case POTRACE_CORNER:
        out.xadj[isegment+1] += 2;
        break;
      case POTRACE_CURVETO:
        out.xadj[isegment+1] += n_points_per_bezier;
        break;
      }
    }
    ++isegment;
    p = p->next;
  }

  // Start sampling
  // Count number of points to output
  out.points = calloc(out.xadj[ out.n_segment ], 2*sizeof(double));

  p = st->plist;
  isegment = 0;
  ipoint = 0;
  while (p != NULL) {
    n = p->curve.n;
    tag = p->curve.tag;
    c = p->curve.c;
    assert( ipoint == out.xadj[isegment] );
    for (i=0; i<n; i++) {
      switch (tag[i]) {
      case POTRACE_CORNER:
        out.points[2*ipoint+0] = c[i][1].x;
        out.points[2*ipoint+1] = c[i][1].y;
        ++ipoint;
        out.points[2*ipoint+0] = c[i][2].x;
        out.points[2*ipoint+1] = c[i][2].y;
        ++ipoint;
        break;
      case POTRACE_CURVETO:
        if(i==0)     ctrl_pts[0] = c[n-1][2];
        else         ctrl_pts[0] = c[i-1][2];
        ctrl_pts[1] = c[i][0];
        ctrl_pts[2] = c[i][1];
        ctrl_pts[3] = c[i][2];
        // Don't write the first point so j starts from one
        for (j = 0 ; j< n_points_per_bezier ; ++j) {          
          double t = 1. / (n_points_per_bezier+1) * (j+1);
          potrace_dpoint_t pt = bezier_value_at(ctrl_pts, t); 
          out.points[2*ipoint+0] = pt.x;
          out.points[2*ipoint+1] = pt.y;
          ++ipoint;
        }
        break;
      }
    }
    ++isegment;
    p = p->next;
  }

  
  // Free used memory
  potrace_state_free(st);

  return out;
}

static void write_polylines(FILE *fl, polyline_list_t pts, int width, int height){
  int isegment, ipoint;

  fprintf(fl, "%%!PS-Adobe-3.0 EPSF-3.0\n");
  fprintf(fl, "%%%%BoundingBox: 0 0 %d %d\n", width, height);
  fprintf(fl, "gsave\n");
  for (isegment = 0; isegment < pts.n_segment; ++isegment) {
    int beg0 =
        (isegment == 0 ? pts.xadj[pts.n_segment] - 1 : pts.xadj[isegment] - 1);
    int beg1 = pts.xadj[isegment];
    int end = pts.xadj[isegment + 1];

    fprintf(fl, "%lf %lf moveto\n", pts.points[2 * beg0 + 0],
           pts.points[2 * beg0 + 1]);
    for (ipoint = beg1; ipoint < end; ++ipoint) {
      fprintf(fl, "%f %f lineto\n", pts.points[2 * ipoint + 0],
             pts.points[2 * ipoint + 1]);
    }
    if (isegment == pts.n_segment - 1 || pts.sign[isegment + 1] == '+') {
      fprintf(fl, "0 setgray fill\n");
    }
  }
  fprintf(fl, "grestore\n");
  fprintf(fl, "%%EOF\n");
  fflush(fl);
}

// ====================================================================== 
// TEST CORE TRACING FUNCTION
// ======================================================================

// #define TEST_CORE_TRACING
#ifdef TEST_CORE_TRACING
int main(){
  const int width = 512;
  const int height = 512;
  int pixel_vals[width*height];
  potrace_param_t *param = potrace_param_default();
  int x,y, bit_value, isegment, ipoint;
  polyline_list_t pts;
  
  for (y=0; y<height; y++) {
    for (x=0; x<width; x++) {
      bit_value = ((x-256)*(x-256)+ (y-256)*(y-256) < 10000) ? 1 : 0;      
      pixel_vals[y*width+x] = bit_value;
    }
  }

  param->turdsize = 0;

  pts = trace(width, height, pixel_vals, param);

 // DRAW THE CURVE
  write_polylines(pts, width, height);

  polyline_list_destroy(pts);
  potrace_param_free(param);

  return 0;
}
#endif

// ====================================================================== 
// WRAP TRACING FUNCTIONS FOR PYTHON
// ====================================================================== 
#define WRAP_FOR_PYTHON
#ifdef WRAP_FOR_PYTHON

#include <Python.h>
// https://docs.python.org/3/extending/building.html
// https://docs.python.org/3/extending/extending.html
// https://martinopilia.com/posts/2018/09/15/building-python-extension.html
// https://github.com/starnight/python-c-extension/

static PyObject *
py_trace(PyObject * self, PyObject * args)
{
  int width = -1;
  int height = -1;
  int *pixel_vals = NULL;
  potrace_param_t *param = potrace_param_default();
  polyline_list_t segments = polyline_list_create();
  PyObject * py_segments = NULL;
  PyObject *dict = NULL;
  int verbose = 1;

  //
  // parse arguments
  //
  if (!PyArg_ParseTuple(args, "O", &dict)) {
    fprintf(stderr, "[Error] Input is not python object \n");
    return NULL;
  }
  if (!PyDict_Check(dict)) {
    fprintf(stderr, "[Error] Input is not python dictionary \n");
    return NULL;
  }

  {
    PyObject *py_str_turdsize = PyUnicode_FromString("turdsize");
    PyObject *py_str_turnpolicy = PyUnicode_FromString("turnpolicy");
    PyObject *py_str_alphamax = PyUnicode_FromString("alphamax");
    PyObject *py_str_opticurve = PyUnicode_FromString("opticurve");
    PyObject *py_str_opttolerance = PyUnicode_FromString("opttolerance");
    PyObject *py_str_width = PyUnicode_FromString("width");
    PyObject *py_str_height = PyUnicode_FromString("height");
    PyObject *py_str_pixels = PyUnicode_FromString("pixels");
    PyObject *py_str_verbose = PyUnicode_FromString("verbose");
    //
    PyObject *py_val_turdsize = PyDict_GetItem(dict, py_str_turdsize);
    PyObject *py_val_turnpolicy = PyDict_GetItem(dict, py_str_turnpolicy);
    PyObject *py_val_alphamax = PyDict_GetItem(dict, py_str_alphamax);
    PyObject *py_val_opticurve = PyDict_GetItem(dict, py_str_opticurve);
    PyObject *py_val_opttolerance = PyDict_GetItem(dict, py_str_opttolerance);
    PyObject *py_val_width = PyDict_GetItem(dict, py_str_width);
    PyObject *py_val_height = PyDict_GetItem(dict, py_str_height);
    PyObject *py_val_pixels = PyDict_GetItem(dict, py_str_pixels);
    PyObject *py_val_verbose = PyDict_GetItem(dict, py_str_verbose);
    //
    if( py_val_verbose ) {
      verbose = (int)PyLong_AsLong( py_val_verbose );
      if(verbose) fprintf(stderr, "verbose  = %d \n", verbose );
    }
    if( py_val_turdsize ) {
      param->turdsize = (int)PyLong_AsLong( py_val_turdsize );
      if(verbose) fprintf(stderr, "turdsize  = %d \n", param->turdsize );
    }
    if( py_val_turnpolicy ) {
      param->turnpolicy = (int)PyLong_AsLong( py_val_turnpolicy );
      if(verbose) fprintf(stderr, "turnpolicy  = %d \n", param->turnpolicy);
    }
    if( py_val_alphamax ) {
      param->alphamax = (double)PyFloat_AS_DOUBLE( py_val_alphamax );
      if(verbose) fprintf(stderr, "alphamax  = %lf \n", param->alphamax);
    }
    if( py_val_opticurve ) {
      param->opticurve = (int)PyLong_AsLong( py_val_opticurve );
      if(verbose) fprintf(stderr, "opticurve  = %d \n", param->opticurve);
    }
    if( py_val_opttolerance ) {
      param->opttolerance = (int)PyLong_AsDouble( py_val_opttolerance);
      if(verbose) fprintf(stderr, "opttolerance  = %lf \n", param->opttolerance);
    }
    if( py_val_height ) {
      height = (int)PyLong_AsLong( py_val_height );
      if(verbose) fprintf(stderr, "height  = %d \n", height);
    }else{
      fprintf(stderr, "[Error] height not given \n");
      return NULL;
    }

    if( py_val_width ) {
      width = (int)PyLong_AsLong( py_val_width );
      if(verbose) fprintf(stderr, "width  = %d \n", width);
    }else{
      fprintf(stderr, "[Error] width not given \n");
      return NULL;
    }

    pixel_vals = calloc(height*width, sizeof(int));

    if (py_val_pixels) {
      int current_item =0;
      PyObject *py_current_pixel;
   
      if (!PySequence_Check(py_val_pixels)) {
        fprintf(stderr, "[Error] inp[pixels] should be iterable \n");
        return NULL;
      }

      for(int y = 0 ; y < height ; ++y){
        for(int x = 0 ; x < width ; ++x){
          py_current_pixel = PySequence_GetItem(py_val_pixels, current_item);

          if (!py_current_pixel) {
            fprintf(stderr, "[Error] pixels[%d] is NULL \n", current_item);
            return NULL;
          }

          if (!PyLong_Check(py_current_pixel)) {
            fprintf(stderr, "[Error] pixels[%d] is not an integer\n", current_item);
            return NULL;
          }
                    
          pixel_vals[y*width+x] = (int)PyLong_AsLong(py_current_pixel);
          ++ current_item; 
          Py_DECREF (  py_current_pixel );
        } 
      } 

      if(verbose) fprintf(stderr, "parsed %d pixels \n", current_item);
    }
    else{
      fprintf(stderr, "[Error] pixels are not given \n");
      return NULL;
    }


    // clean up !
    // the py_val_* stuff are borrowed, so don't reduce reference for them
    Py_DECREF(py_str_turdsize);
    Py_DECREF(py_str_turnpolicy);
    Py_DECREF(py_str_alphamax);
    Py_DECREF(py_str_opticurve);
    Py_DECREF(py_str_opttolerance);
    Py_DECREF(py_str_pixels);
    Py_DECREF(py_str_verbose);
    //
  }

  assert( width  > 1 );
  assert( height  > 1 );
  assert (pixel_vals);

  //
  // TRACE
  //
  segments = trace(width, height, pixel_vals, param);

  //
  // Debug
  //
  if(0) write_polylines(stdout, segments, width, height);

  //
  // Create python return object
  // 
  // int n_segment;
  // int *sign;
  // int *xadj;
  // double *points;
 
  {
    py_segments = PyDict_New();

    // create the values
    PyObject *py_val_n_segments = PyLong_FromLong((long)segments.n_segment);
    PyObject *py_val_sign = PyList_New(segments.n_segment);
    PyObject *py_val_xadj = PyList_New(segments.n_segment+1);
    PyObject *py_val_points = PyList_New(2*segments.xadj[segments.n_segment]);

    // add the values to the dict
    PyDict_SetItemString(py_segments, "n_segment", py_val_n_segments);
    PyDict_SetItemString(py_segments, "sign", py_val_sign);
    PyDict_SetItemString(py_segments, "xadj", py_val_xadj);
    PyDict_SetItemString(py_segments, "points", py_val_points);

    // Add values to list
    for (int isegment =0 ; isegment < segments.n_segment ; ++isegment){
      PyObject *entry = PyLong_FromLong(segments.sign[ isegment ]);
      PyList_SetItem(py_val_sign, isegment, entry); // steals the reference
    }
    for (int isegment =0 ; isegment < segments.n_segment+1 ; ++isegment){
      PyObject *entry = PyLong_FromLong(segments.xadj[ isegment ]);
      PyList_SetItem(py_val_xadj, isegment, entry); // steals the reference
    }
    for (int ipoint =0 ; ipoint < 2*segments.xadj[segments.n_segment] ; ++ipoint){
      PyObject *entry = PyFloat_FromDouble( segments.points[ipoint]);
      PyList_SetItem(py_val_points, ipoint, entry); // steals the reference
    }

  }

  //
  // Clean up
  //
  potrace_param_free(param);
  polyline_list_destroy(segments);
  free(pixel_vals);

  //
  // Return
  //
  return py_segments;
}

static PyMethodDef PyTraceMethods[] = {
 { "py_trace", (PyCFunction)py_trace, METH_VARARGS, "Trace some pixels" },
 { NULL, NULL, 0, NULL }
};

static struct PyModuleDef PyTraceModule = {
    PyModuleDef_HEAD_INIT,
    "py_trace",   // name of module 
    NULL, // module documentation, may be NULL 
    -1,   // size of per-interpreter state of the module,  or -1 if the module keeps state in global variables. 
    PyTraceMethods
};

PyMODINIT_FUNC
PyInit_py_trace(void){
  return PyModule_Create(&PyTraceModule);
}

#endif
