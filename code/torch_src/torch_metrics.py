import numpy as np

def predict_label(proba):
    return np.argmax(proba, axis=1).flatten()
    
def accuracy(gt_label, pred_label):
    return np.sum(gt_label == pred_label) / pred_label.shape[0]

def confusion_matrix(gt_label, pred_label):
    n_labels = np.amax( (np.amax( gt_label ),  np.amax(pred_label)) ) + 1
    conf_mat = np.zeros( (n_labels, n_labels) , dtype=int)
    
    for i in range(gt_label.shape[0]):
        ground_truth = gt_label[i]
        prediction = pred_label[i]
        conf_mat[ground_truth, prediction] += 1
 
    return conf_mat

def precision_recall( conf_mat ):

    precision = np.zeros(conf_mat.shape[0]) 
    recall = np.zeros(conf_mat.shape[0])
    
    for i in range(conf_mat.shape[0]):
        TP = conf_mat[i,i]
        FN = np.sum( conf_mat[i,:] ) - conf_mat[i,i]
        FP = np.sum( conf_mat[:,i] ) - conf_mat[i,i] 
        precision[i] = TP / (TP+FP)
        recall[i] = TP / (TP+FN)

    return precision, recall
