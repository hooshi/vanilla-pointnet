import random
import torch
import numpy as np
import datetime
import pickle

def set_seed(seed): 
    random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.cpu.manual_seed(seed)
    torch.cpu.manual_seed_all(seed)
    np.random.RandomState(seed)
    torch.backends.cudnn.deterministic=True
    torch.backends.mklnn.deterministic=True
    torch.backends.mkl.deterministic=True

def get_time():
    return str(datetime.datetime.now()).rsplit(":",1)[0].replace(" ", "-").replace(":","")

def save_obj(model, filename):
    torch.save( model, filename )

def load_obj(filename):
    return torch.load( filename )

## Will not work well
def save_obj_old(model, filename):
    with open(filename, 'wb') as fl:
        pickle.dump( obj=model, file=fl, protocol=3 )

def load_obj_old(filename):
    with open(filename, 'rb') as fl:
        return pickle.load( fl )
