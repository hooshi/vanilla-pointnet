from tqdm import tqdm
import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.optim as optim
#from predict_models import eval_net

import sys
import torch_predict
import torch_metrics

def train(net,
          loss_vec,
          acc_vec,
          dataset,
          val_dataset,
          device,
          print_every=1000,
          eval_every=6000,
          num_epochs=1,
          seed=1,
          batch_size=1,
          optimizer=None):
    
    net.to(device)
    net.train()
    # optimizer = optimizer.cuda()
    criterion = nn.CrossEntropyLoss()

    if batch_size > 1:
        assert dataset.is_batchable(), "Data set only supports batch size 1"
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=8)


    ii = 0
    jj = 0
    for epoch in range(num_epochs):  # loop over the dataset multiple times
        running_loss = 0.0
        for data in  dataloader:
            optimizer.zero_grad()
            
            inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.view(-1).to(device)
            
            outputs = net(inputs).to(device)
            #outputs = outputs.view(1, -1)
            
            loss = criterion(outputs, labels)

            ##
            loss.backward()
            optimizer.step()

            running_loss += loss.item()
            
            ii += inputs.shape[0]
            jj += inputs.shape[0]
            
            if jj > print_every :
                n_seen_items = ii % eval_every+1
                print('[%d, %5d] loss: %15g' % (epoch + 1, n_seen_items, running_loss/n_seen_items*batch_size))
                if(n_seen_items!=1):
                    loss_vec.append(running_loss/n_seen_items*batch_size)
                jj=0

            if (ii % eval_every == 0) and ( len(val_dataset) > 0 ):
                print("Evaluating classification accuracy ...")
                gt_label, pred_proba = torch_predict.predict(net, val_dataset, device)
                pred_label = torch_metrics.predict_label( pred_proba )
                acc = torch_metrics.accuracy(gt_label, pred_label)
                acc_vec.append(acc)
                net.train()
                print("Acc = %.4f" %(acc))
                running_loss = 0.0
                
    print('Finished Training')
    return (loss_vec, acc_vec)
