import torch
import numpy as np
from tqdm import tqdm


## Returns
## True labels, predicted probabilities
def predict(net, dataset, device):
    net.eval() # set to eval mode
    net.to(device)
    pred_label_vec = [None]*len(dataset)
    gt_label_vec = [None]*len(dataset)
    with torch.no_grad(): 
        for i in tqdm(range(len(dataset))):
            points, gt_label = dataset[i]
            points = points.to(device)            
            pred_label = net(points).flatten()
            pred_label_vec[i] = [pred_label[i].item() for i in range(len(pred_label))]
            gt_label_vec[i] = gt_label.item()

    pred_label_vec = np.array(pred_label_vec).reshape(len(dataset), -1)
    gt_label_vec = np.array(gt_label_vec).flatten()
    return gt_label_vec, pred_label_vec

