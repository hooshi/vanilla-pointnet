#
# Load the rapperArm animations in Paraview.  Or just generate
# videos without launching the Paraview GUI, using the python
# interface (easier to use the python shipped with Paraview --
# pvpython).
#
# Works with all MSYS+bash, Linux bash, and the garbage couple
# cmd/powershell.
#
# To use,  add the bin directory of Paraview to your path.
#
# Author: Shayan Hoshyari
#

import os
import glob
import numpy as np

# regular expression for settings properties as group
import re

# My dear friend paraview!
from paraview.simple import *
import paraview.servermanager 

# =================================================================
#    PREDEFINED CAMERA POSITIONS
# =================================================================
ANGLES = {}

# A FAIRLY GOOD VIEW, ADD MORE BELOW
ANGLES[0] = dict(
CameraPosition = [87.86524958671663, -56.87552421119497, 125.38632068564257],
CameraFocalPoint = [0.0, 0.0, 0.0],
CameraViewUp= [-0.6139490260029544, 0.4621911737354975, 0.6398796077318193],
CameraParallelScale= 42.2730476306,
ViewSize= [700, 700],
)

ANGLES['bathtub'] = dict(
CameraPosition = [-67.70729488920594, -57.867792924513125, 101.42826465476476],
CameraFocalPoint = [0.0, 0.0, 0.0],
CameraViewUp= [0.7299423922028441, 0.2564480809919646, 0.6335759511066753],
CameraParallelScale= 42.2730476306,
ViewSize= [700, 700],
)

ANGLES['bathtub'] = dict(
CameraPosition = [-67.70729488920594, -57.867792924513125, 101.42826465476476],
CameraFocalPoint = [0.0, 0.0, 0.0],
CameraViewUp= [0.7299423922028441, 0.2564480809919646, 0.6335759511066753],
CameraParallelScale= 42.2730476306,
ViewSize= [700, 700],
)

ANGLES['bed'] = dict(
CameraPosition = [-249.78961089202787, -162.8159617858455, 128.421615187643],
CameraFocalPoint = [0.0, 0.0, 0.0],
CameraViewUp= [0.3972142504138533, 0.10930682599559262, 0.911193095374921],
CameraParallelScale= 84.0249555913,
ViewSize= [700, 700],
)

ANGLES['chair'] = ANGLES['bed']

ANGLES['desk'] = dict(
CameraPosition = [-128.16058181302975, -56.91759463505037, 36.634798967544754],
CameraFocalPoint = [0.0, 0.0, 0.0],
CameraViewUp= [0.257142524960127, 0.042399390736433236, 0.9654429105453672],
CameraParallelScale= 37.5125647233,
ViewSize= [700, 700],
)

ANGLES['dresser'] = ANGLES['desk'] 

ANGLES['monitor']= dict(
CameraPosition = [-49.60703505219721, -19.973339919772563, 14.089839537993411],
CameraFocalPoint = [0.0, 0.0, 0.0],
CameraViewUp= [0.2571425249601269, 0.04239939073643324, 0.9654429105453671],
CameraParallelScale= 37.1248106468,
ViewSize= [700, 700],
)

ANGLES['night_stand'] = ANGLES['monitor']
ANGLES['sofa'] = ANGLES['monitor']
ANGLES['table'] = ANGLES['monitor']
ANGLES['toilet'] = ANGLES['monitor']

# =================================================================
#    UTILITY
# =================================================================
def remove_all():
    previous_sources = GetSources().keys()
    for k in previous_sources:
        print ("Deleting {}".format(k))
        proxy = FindSource(k[0])
        try:    Delete(proxy)
        except: pass

def print_camera():
    renderView1 = GetActiveViewOrCreate('RenderView')
    print('dict(')
    print('CameraPosition = {},'.format(renderView1.CameraPosition))
    print('CameraFocalPoint = {},'.format(renderView1.CameraFocalPoint))
    print('CameraViewUp= {},'.format(renderView1.CameraViewUp))
    print('CameraParallelScale= {},'.format(renderView1.CameraParallelScale))
    print('ViewSize= {},'.format(renderView1.ViewSize))
    print(')')

    
def set_camera(camera):
    render_view = GetActiveViewOrCreate('RenderView')
    if(camera is None):
        render_view.ResetCamera()
        return
    render_view.CameraPosition = camera['CameraPosition']
    render_view.CameraFocalPoint = camera['CameraFocalPoint']
    render_view.CameraViewUp= camera['CameraViewUp']
    render_view.CameraParallelScale= camera['CameraParallelScale']
    if('ViewSize' in camera): render_view.ViewSize= camera['ViewSize']

def set_source_properties(source_name, **options):
    render_view = GetActiveViewOrCreate('RenderView')
    proxy = FindSource(source_name)
    #proxy_display = Show(proxy, render_view)
    proxy_display = GetDisplayProperties(proxy, view=render_view)
    if('RepresentationType' in options):
        proxy_display.SetRepresentationType(options['RepresentationType'])
    if('DiffuseColor' in options):
        proxy_display.DiffuseColor = options['DiffuseColor']
    if('Opacity' in options):
        proxy_display.Opacity = options['Opacity']
    if('PointSize' in options):
        proxy_display.PointSize =  options['PointSize']
    if('RenderPointsAsSpheres' in options):
        proxy_display.RenderPointsAsSpheres = options['RenderPointsAsSpheres']

def set_source_properties_group(name_pattern, **options):
    source_names = GetSources().keys()
    for (source_name, _) in source_names:
        if re.search(name_pattern, source_name):
            set_source_properties(source_name, **options)


def set_animation_length(val):
    animationScene = GetAnimationScene()
    animationScene.PlayMode = 'Real Time'
    animationScene.Duration = val

def save_animation(fname, frame_rate, length):
    set_animation_length(int(length))
    UpdatePipeline() 
    render_view = GetActiveViewOrCreate('RenderView')
    nx=render_view.ViewSize[0]
    ny=render_view.ViewSize[1]
    SaveAnimation(fname,FrameRate=frame_rate, FrameWindow=[0, int(frame_rate*length)], ImageResolution =[2*nx, 2*ny])

def save_screenshot(fname):
    UpdatePipeline() 
    render_view = GetActiveViewOrCreate('RenderView')
    nx=render_view.ViewSize[0]
    ny=render_view.ViewSize[1]
    SaveScreenshot(fname, ImageResolution =[2*nx, 2*ny], TransparentBackground=1)


def load_vtu(filename, out_name):
    input_mesh = XMLUnstructuredGridReader(FileName=filename)
    render_view = GetActiveViewOrCreate('RenderView')
    RenameSource(out_name, input_mesh)
    Show()
    return input_mesh

    
# =======================================================
#         DIRTY WORK
# =======================================================
def process_case(case, out_name, camera_angle, is_pc=False):
    remove_all()
    
    source_name = 'model'
    load_vtu(case, source_name)

    # Hide the points
    if(is_pc):
        set_source_properties(source_name, PointSize=14.0, RenderPointsAsSpheres=1)
        set_source_properties(source_name, DiffuseColor=(0.5,0.5,0.7))
    else:
        set_source_properties(source_name, PointSize=0.01)
        set_source_properties(source_name, DiffuseColor=(0.7,0.7,0.7))
    Render()
    
    # Reset the camera
    set_camera(camera_angle)
    render_view = GetActiveViewOrCreate('RenderView')
    render_view.ResetCamera()

    # Hide the orientaiton axis
    render_view.OrientationAxesVisibility = 0

    # Take a screenshot
    print( out_name )
    save_screenshot( out_name )

classes = [
    'bathtub',
    'chair',
    'dresser',  
    'night_stand',
    'sofa',   
    'toilet',
    'bed',    
    'desk',
    'monitor',
    'table',
]


# for class_name in classes:
#     for i in range(1,10):
#         prefix = '/home/hooshi/code/cs540-shared-bup/project/code/datasets/MODELNET/raw_data/ModelNet10/'
#         case = '{prefix}/{class_name}/train/{class_name}_{no:04d}.ref.vtu'.format(prefix=prefix, class_name=class_name, no=i)
#         print( case )
#         if not os.path.exists('screenshot'): os.makedirs('screenshot')
#         png_name = case
#         png_name = png_name.rsplit('/',1)[-1]
#         png_name = png_name.split('.',1)[0]
#         outname = 'screenshot/{}.png'.format(png_name)
#         process_case( case, outname, ANGLES[class_name] )
        

prefix = '/home/hooshi/code/cs540-shared-bup/project/code/datasets/MODELNET/raw_data/'
for pcsize in [256, 512, 1024, 2048, 4096]:
    case ='{prefix}/ModelNet10_PC{pcsize:04d}/{class_name}/train/{class_name}_{no:04d}.vtu'.format(prefix=prefix,pcsize=pcsize, class_name='chair', no=17)
    process_case( case, 'screenshot/pc{}.png'.format(pcsize), ANGLES['chair'], True )
case ='{prefix}/ModelNet10/{class_name}/train/{class_name}_{no:04d}.ref.vtu'.format(prefix=prefix,class_name='chair', no=17)
process_case( case, 'screenshot/mesh.png', ANGLES['chair'], False )
