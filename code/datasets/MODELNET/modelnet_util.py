#
# Some basic functionality to read and write meshes in python
#
# Needs the lxml package
# pip install lxml
#

import sys
import os
import pickle
import bisect
import random

import numpy as np

sys.path.append(os.getcwd())
import defs
sys.path.append(defs.MESHIO_PATH)
import meshio


# ========================================================================
#
# Reading and writing meshes and point clouds
#
# ========================================================================

# Read a .off (.OFF) surface mesh file.
#
# filename: name of .off file to read
# returns V, F
# V is a nx3 float matrix of n point locations
# F is a mx3 int matrix of the index of 3 vertices for the m faces
def io_read_off(filename):
    meshio_mesh = meshio.off_io.read( filename )
    V = meshio_mesh.points
    F = meshio_mesh.cells["triangle"]

    V= np.array(V, dtype=float)
    F= np.array(F, dtype=int)
    
    assert V.shape[1]==3, "{} == 3".format(V.shape[1])
    assert F.shape[1]==3, "{} == 3".format(F.shape[1])

    return V, F

# Read a .vtu  file (only parses points and triangles).
#
# filename: name of .vtu file to read
# returns V, F
# V is a nx3 float matrix of n point locations
# F is a mx3 int matrix of the index of 3 vertices for the m faces
def io_read_vtu(filename):

    meshio_mesh = meshio.vtu_io.read( filename )
    
    if "triangle" in meshio_mesh.cells:
        F = meshio_mesh.cells["triangle"]
        F= np.array(F, dtype=int)
        assert F.shape[1]==3, "{} == 3".format(F.shape[1])
    else:
        F = None

    V = meshio_mesh.points
    V= np.array(V, dtype=float)
    assert V.shape[1]==3, "{} == 3".format(V.shape[1])

    return V, F

# Write a .vtu  surface mesh file.
# This mesh can be openned and visualized using paraview.
#
# Input:
#  V is a nx3 float matrix of n point locations
#  F is a mx3 int matrix of the index of 3 vertices for the m faces
#  You can also write per point or per face data which paraview an view
#  V_data is a dictionary, where V_data['data_name'] = a numpy vector of len n
#  F_data is a dictionary, where V_data['data_name'] = a numpy vector of len m

def io_write_vtu(filename, V, F=None, V_data=None, F_data=None):
    # Check types
    assert isinstance(V,np.ndarray)
    if F is None: F = np.zeros( (0,3), dtype=int)
    assert isinstance(F,np.ndarray)
    if V_data is None: V_data = {} 
    if F_data is None: F_data = {} 
    assert isinstance(V_data, dict)
    assert isinstance(F_data, dict)

    # check dims
    assert V.shape[1] == 3
    assert F.shape[1] == 3
    for _ , value in V_data.items():
        assert isinstance(value, np.ndarray)
        assert value.shape[0] == V.shape[0], "{} == {}".format(value.shape[0], V.shape[0])
    for _ , value in F_data.items():
        assert isinstance(value, np.ndarray)
        assert value.shape[0] == F.shape[0], "{} == {}".format(value.shape[0], F.shape[0])
        
    
    # create meshio mesh

    ## Add elements
    _cells = {}
    if 1:
        _cells["vertex"] = np.arange(V.shape[0]).reshape(-1,1)
    if F.shape[0]>0:
        _cells["triangle"] = F

    # Data should be present in both faces and vertices
    _cell_data = {}
    _cell_data["vertex"] = V_data
    _cell_data["triangle"] =  F_data
    for key in V_data:
        if key not in F_data:
            F_data[key] = np.zeros( F.shape[0] ) 
    for key in F_data:
        if key not in V_data:
            V_data[key] = np.zeros( V.shape[0] ) 

    # Create the mesh
    meshio_mesh = meshio.Mesh(
        points = V,
        cells = _cells,
        cell_data=  _cell_data,
    )

    # write the mesh
    meshio.vtu_io.write(filename,
                        meshio_mesh,
                        write_binary=True,
    )


# Don't use for torch objects
def io_write_pickle( obj, filename ):
    with open(filename, 'wb') as fl:
        pickle.dump( obj=obj, file=fl, protocol=defs.PICKLE_PROTOCOL )
        
def io_read_pickle( filename ):
    with open(filename, 'rb') as fl:
        obj = pickle.load( fl )
    return obj

# ========================================================================
#
# SAMPLING
#
# ========================================================================

# Sample a point within a triangle
# https://math.stackexchange.com/questions/18686/uniform-random-point-in-triangle
# V: 3x3 matrix, each row is a vertex
def geo_sample_triangle(V):
    r12 = np.random.random(2)
    r2 = r12[1]
    r1sq = np.sqrt(r12[0])
    return (1-r1sq)*V[0,:] + r1sq*(1-r2)*V[1,:] + r2*r1sq*V[2,:]


# V: 3x3 matrix, each row is a vertex
def geo_triangle_area(V):
    ab = V[1,:] - V[0,:]
    ac = V[2,:] - V[0,:]
    return 0.5 * np.linalg.norm( np.cross(ab, ac) )

# Downsample pointset
def geo_downsample_points_by_distance(V, eps):
    V_int = np.round(V/float(eps)).astype(int)
    _, unique_ids = np.unique(V_int, return_index=True, axis=0)    
    V_sampled = V[unique_ids, :]
    return V_sampled

def geo_downsample_points_randomly(V, n_target):
    assert V.shape[0] > n_target
    ids = np.random.choice( range(V.shape[0]) , size=n_target, replace=False)
    return V[ids, :]

def geo_upsample_points_randomly(V, n_target):
    assert V.shape[0] < n_target
    n_missing = n_target - V.shape[0]
    ids = np.random.choice( range(V.shape[0]) , size=n_missing, replace=True)
    new_ids = np.concatenate( (range(V.shape[0]), ids) )
    return V[new_ids, :]

# Sample a mesh with points
def geo_sample_mesh_naive(V, F, n_target_points):
    # Find the areas
    F_areas = np.zeros( F.shape[0] )
    for face_id in range(F.shape[0]):
        F_areas[face_id] = geo_triangle_area( V[ F[face_id,:], : ] )
    proba = F_areas / np.sum( F_areas )
    # sample the faces
    V_sampled = np.zeros( (n_target_points, 3), dtype=float)
    faces_ids = np.random.choice( range(F.shape[0]) , size=n_target_points, replace=True, p=proba)
    for sample_id, face_id in enumerate(faces_ids):
        V_sampled[sample_id,:] = geo_sample_triangle( V[ F[face_id,:], : ] )

    return V_sampled

# Sample a mesh with points
def geo_sample_mesh_high_quality(V, F, n_target_points):
    safety_factor=20
    max_power_of_two = 10
    
    # First sample with a much higher resolution    
    V_highres = geo_sample_mesh_naive(V,F, n_target_points*safety_factor)
    
    # Now get the largest dimension
    bbox = geo_bbox(V_highres)
    dim_max = np.max(bbox[1]-bbox[0])
    
    # Now keep binning until the number of binned points are
    # bigger than target
    success= False
    for i in range(max_power_of_two):
        eps = (2. ** -i)*dim_max
        V_sample = geo_downsample_points_by_distance(V_highres, eps)
        # print("{}: {}".format( eps, V_sample.shape[0] ))
        if V_sample.shape[0] > n_target_points:
            V_sample = geo_downsample_points_randomly(V_sample, n_target_points)
            success=True
            break
            
    if not success:
        assert 0, "Failure :(, final size: {} < {}".format( V_sample.shape[0], n_target_points )
            
    return V_sample

# ========================================================================
#
# RESIZING
#
# ========================================================================

# Get the bounding box of a set of points
def geo_bbox(V):
    max_pt = np.max(V, axis=0)
    min_pt = np.min(V, axis=0)
    return (min_pt, max_pt)

# Shift the center of a point cloud to 0,0,0
def geo_centered_points(V):
    return V - np.mean(V, axis=0)

# Scale a pointcloud so that its maximum dimension is 1
def geo_scaled_points(V):
    #max_pt, min_pt = geo_bbox(V)
    #sides = max_pt - min_pt
    #max_side = np.max(sides)
    rr2 = np.sum(V * V, axis=1)
    max_rr2 = np.max(rr2)
    max_rr = np.sqrt( max_rr2 )
    return V /  max_rr
    
# =======================================================
# Create the indices for training and validation set
# Assumes that the training dataset is loaded
# returns (training_ids, validation_ids)
# =======================================================
def ml_training_and_validation_indices(n_samples, validation_ratio=0.2):
    all_ids = np.arange( n_samples )
    ## Shuffle with predictive seed
    rnd = np.random.RandomState(12312332)
    rnd.shuffle( all_ids )
    ##
    n_training_samples = int( (1-validation_ratio) * n_samples )
    training_ids  = all_ids[:n_training_samples]
    validation_ids  = all_ids[n_training_samples:]
    ##
    return (training_ids, validation_ids)

def ml_sub_dataset(dataset_dict, indices):
    subset = {
        "samples": dataset_dict["samples"][indices,:,:],
        "labels": dataset_dict["labels"][indices],
        "labels_as_string": dataset_dict["labels_as_string"]
    }
    return subset

# ========================================================================
#
# TESTS
#
# ========================================================================

# Test reading an off file
def test_read_off():
    print ("======= Testing reading .off file")
    
    #
    #  (0)--(1)
    #   | \  |
    #   |  \ |
    #  (3)--(2)
    dummy_mesh_content = """OFF
4 2 0
0. 1. 0.
1. 1. 0.
1. 0. 0.
0. 0. 0.
3 0 2 1
3 3 2 0 
    """
    with open("test/_dummy.off", "w") as ff:
        ff.write(dummy_mesh_content)
    V,F = io_read_off( "test/_dummy.off" )
    print(dummy_mesh_content)
    print('was parsed as')
    print('V: ', V )
    print('F: ', F )

# Test reading a vtu
def test_read_vtu():
    print ("======= Testing reading .vtu file")
    
    V = [
        [0., 1., 0.],
        [1., 1., 0.],
        [1., 0., 0.],
        [0., 0., 0.]
    ]
    F = [
        [0,2,1],
        [3,2,0]
    ]
    V= np.array(V)
    F = np.array(F)

    io_write_vtu("test/_dummy.off", V, F)
    Vn, Fn = io_read_vtu("test/_dummy.off")
    assert( np.max( (V - Vn).flatten() ) < 1e-10 )
    assert( np.max( (F - Fn).flatten() ) < 1e-10 )
    ##
    io_write_vtu("test/_dummy.off", V)
    Vn, Fn = io_read_vtu("test/_dummy.off")
    assert( np.max( (V - Vn).flatten() ) < 1e-10 )
    assert( Fn is None )

    
# test writing vtu
def test_write_vtu():
    print ("======= Testing writing .vtu file")

    V = [
        [0., 1., 0.],
        [1., 1., 0.],
        [1., 0., 0.],
        [0., 0., 0.]
    ]
    F = [
        [0,2,1],
        [3,2,0]
    ]
    V= np.array(V)
    F = np.array(F)

    V_data = {
        "data1": np.array([9.,10.,11.,12.]),
        "data2": np.array([5.,3.,1.,4.]),
    }

    F_data = {
        "data3": np.array([9.,10.]),
        "data4": np.array([5.,3.]),
    }

    io_write_vtu("test/_dummy_pointcloud_nodata.vtu",V=V)
    io_write_vtu("test/_dummy_pointcloud_wdata.vtu", V=V, V_data=V_data)
    io_write_vtu("test/_dummy_mesh_nodata.vtu", V=V, F=F)
    io_write_vtu("test/_dummy_mesh_wdata.vtu", V=V, F=F, V_data=V_data, F_data=F_data)
    io_write_vtu("test/_dummy_mesh_wfdata.vtu", V=V, F=F, F_data=F_data)
    io_write_vtu("test/_dummy_mesh_wvdata.vtu", V=V, F=F, V_data=V_data)

# test triangle area
def test_triangle_area():
    V = np.array([
        [1,1,1],
        [2,1,1],
        [1,2,1],
    ])
    assert np.abs( geo_triangle_area(V) - 0.5) < 1e-10
    
# test sampling a mesh
def test_sample_mesh():
    print ("======= Testing sampling a mesh")
    V,F = io_read_off('raw_data/ModelNet10/bed/train/bed_0001.off')
    #
    io_write_vtu('test/_dummy_before_sampling.vtu', V, F)
    # SAMPLE VARIOUS RESOLUTIONS
    out_id = [0]
    def _perform_sampling(_n_sample):
        out_id[0] += 1
        _V_sample =  geo_sample_mesh_naive(V, F, _n_sample)
        io_write_vtu('test/_dummy_sampled.%04d.vtu'%out_id[0], _V_sample)
    _perform_sampling(256*1)
    _perform_sampling(256*2)
    _perform_sampling(256*4)
    _perform_sampling(256*8)
    _perform_sampling(256*16)
    _perform_sampling(256*32)
    _perform_sampling(256*64)
    # NOW BIN TOGETHER VERTICES OF THE FINEST RESOLUTION
    V_fine =  geo_sample_mesh_naive(V, F, 32768)
    bbox = geo_bbox(V)
    dim_max = np.max(bbox[1]-bbox[0])
    V_bin= geo_downsample_points_by_distance(V_fine, eps=dim_max/32)
    io_write_vtu('test/_dummy_sampled_bined.0.vtu', V_bin)
    V_rand = geo_downsample_points_randomly(V_bin, n_target=1024)
    io_write_vtu('test/_dummy_sampled_bined.1.vtu', V_rand)
    V_rand = geo_upsample_points_randomly(V_rand, n_target=10000)
    io_write_vtu('test/_dummy_sampled_bined.2.vtu', V_rand)
    # NOW SAMPLE SOME OTHER MESHES
    out_id[0] = 0
    def _perform_sampling_hq(filename, _n_sample):
        out_id[0] += 1
        print("sampling high quality %s"%filename)
        V,F = io_read_off(filename)
        _V_sample =  geo_sample_mesh_high_quality(V, F, _n_sample)
        _V_centered = geo_centered_points( _V_sample )
        _V_scaled = geo_scaled_points( _V_centered )
        io_write_vtu('test/_dummy_sampled.%04d.vtu'%out_id[0], _V_scaled)
        np.savetxt('test/_dummy_sampled.%04d.xyz'%out_id[0], _V_scaled)
        
    n_point=1024
    prefix = 'raw_data/ModelNet10/bed/train/'
    _perform_sampling_hq(prefix+'bed_0001.off', n_point)
    _perform_sampling_hq(prefix+'bed_0002.off', n_point)
    _perform_sampling_hq(prefix+'bed_0003.off', n_point)
    _perform_sampling_hq(prefix+'bed_0004.off', n_point)
    _perform_sampling_hq(prefix+'bed_0005.off', n_point)
    _perform_sampling_hq(prefix+'bed_0006.off', n_point)
    _perform_sampling_hq(prefix+'bed_0007.off', n_point)
    _perform_sampling_hq(prefix+'bed_0008.off', n_point)
    _perform_sampling_hq(prefix+'bed_0009.off', n_point)
    _perform_sampling_hq(prefix+'bed_0010.off', n_point)
    _perform_sampling_hq(prefix+'bed_0011.off', n_point)
    _perform_sampling_hq(prefix+'bed_0012.off', n_point)
        
if __name__ == "__main__":
    test_read_off()
    test_write_vtu()
    test_read_vtu()
    test_triangle_area()
    test_sample_mesh()
