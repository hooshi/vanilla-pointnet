#
# Download the processed modelnet data set either directly download the files from these link (there are two):
# 
# VTU FILES can be visualized by paraview:
# https://drive.google.com/file/d/1JeDkdIrBh2EF2i9boVOiXU1qDP4PhGRa/view?usp=sharing
#
# PICKLED DATASETS READY FOR TRAINING
# https://drive.google.com/file/d/1cTuZrVA3STqq9o3Ti1A5xm3FnaWoGUyR/view?usp=sharing
#
# Best model
# https://drive.google.com/file/d/1vAj3OpZBv_ZJ5N151t8QKkm_CTlMdAfs/view?usp=sharing
#
# and unzip it under MODELNET/raw_data/
#
# Or run
# bash drivers/download_tmnist.sh
#


function download_from_gdrive
{
    FILENAME="$2"
    FILEID="$1"

    FILEID2=$(wget --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=${FILEID}" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')
    
    wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=${FILEID2}&id=${FILEID}" -O "${FILENAME}"

    rm -rf /tmp/cookies.txt
}

mkdir -p raw_data

FILEID="1JeDkdIrBh2EF2i9boVOiXU1qDP4PhGRa"
download_from_gdrive ${FILEID} raw_data/ModelNet10VTU.zip
#
FILEID="1cTuZrVA3STqq9o3Ti1A5xm3FnaWoGUyR"
download_from_gdrive ${FILEID} raw_data/ModelNet10Pickle.zip

FILEID="1vAj3OpZBv_ZJ5N151t8QKkm_CTlMdAfs"
download_from_gdrive ${FILEID} raw_data/modelnet10-pointnet-pc1024-noaugmentation.zip

unzip raw_data/ModelNet10Numpy.zip
unzip raw_data/ModelNet10Pickle.zip
unzip raw_data/modelnet10-pointnet-pc1024-noaugmentation.zip
