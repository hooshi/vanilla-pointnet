##
## Eval a trained pointnet model on the model net dataset.
##
## USAGE:
## python3 python3 drivers/eval_pointnet.py [saved-model.py]
##

import sys 
import os

import matplotlib.pyplot as plt
import numpy as np
import torch

sys.path.append(os.getcwd())
import modelnet_torch
import modelnet_util
import defs
sys.path.append(defs.TORCH_SRC_PATH)
import torch_predict
import torch_train
import torch_utils
import torch_metrics



device = torch.device('cpu')
if len(sys.argv) <= 1:
    print("Usage python3 drivers/eval_pointnet.py [saved-model.py]")
    exit(-1)
else:
    pc_size = 1024
    ctx = modelnet_torch.TrainingContext()
    ctx.load( sys.argv[1] )
    

print("Model {}:".format(sys.argv[1]))
ctx.print_info(sys.stdout)

## GET TRAINING AND VALIDATION SETS
_, _, test_set = modelnet_torch.load_dataset(pc_size)
print( "Test set len:", len(test_set) )

# Get accuracy
gt_label, pred_proba = torch_predict.predict(ctx._net, test_set, device)
pred_label = torch_metrics.predict_label( pred_proba )
accuracy = torch_metrics.accuracy(gt_label, pred_label)
print("Test accuracy is: %.4g"%accuracy)

# Get confusion matrix
conf = torch_metrics.confusion_matrix(gt_label, pred_label)
print("Confusion")
print (conf)

# Get precision and recall
precision, recall = torch_metrics.precision_recall(conf)
print("Precision - Recall")
print(np.concatenate((precision.reshape(-1,1), recall.reshape(-1,1)), axis=1) )

print( 'Average accuracy: {:.3g}'.format(np.average(precision)))

test_set_shayans_format = modelnet_util.io_read_pickle( 'raw_data/modelnet10-train-pcsize%d.pickle'%pc_size )
print( test_set_shayans_format['labels_as_string'] )

## PLOT LOSS AND VALIDATION ACCURACY
plt.figure(figsize=(4,2))
plt.subplot(2,1,1)
plt.semilogy(ctx._history_loss)
plt.xticks([])
plt.ylabel("loss")
#
plt.subplot(2,1,2)
plt.plot(ctx._history_vacc)
plt.ylabel("validation accuracy")
plt.xlabel("epoch")
plt.savefig("history.svg", bbox_inches='tight')
plt.show()
