#
# Sample modelnet as a pointcloud so that we can learn on
#

import sys
import os
import glob
import math
 # https://docs.python.org/3/library/multiprocessing.html
import multiprocessing

import numpy as np

sys.path.append(os.getcwd())
import modelnet_util


# HARDCODE INFO
N_PROC = 7
POINTCLOUD_SIZE = None
if(len(sys.argv) < 2):
    print("Usage {} [pointcloudsize]")
    sys.exit(-1)
else:
    POINTCLOUD_SIZE = int(sys.argv[1])
# MUST BOTH END IN /
IN_FOLDER = "raw_data/ModelNet10/"
OUT_FOLDER = "raw_data/ModelNet10_PC%04d/"%POINTCLOUD_SIZE

# CONVERT SINGLE MESH
def convert_single_mesh(inname):
    print('converting {}'.format(inname))

    base_name = inname.rsplit('/')[-1].replace('.off', '')
    dir_name =  inname.replace(IN_FOLDER, OUT_FOLDER).rsplit('/',1)[0]
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)

    #
    outname_xyz = '%s/%s.xyz'%(dir_name, base_name)
    outname_vtu = '%s/%s.vtu'%(dir_name, base_name)
    outname_ref_vtu = '%s/%s.ref.vtu'%(dir_name, base_name)
    
    V,F = modelnet_util.io_read_off(inname)
    # Let's just sample naively
    #V_sample = modelnet_util.geo_sample_mesh_high_quality(V, F, POINTCLOUD_SIZE)
    V_sample = modelnet_util.geo_sample_mesh_naive(V, F, POINTCLOUD_SIZE)
    V_sample = modelnet_util.geo_centered_points( V_sample )
    V_sample = modelnet_util.geo_scaled_points( V_sample )

    #
    #modelnet_util.io_write_vtu(outname_ref_vtu, V=V, F=F)
    modelnet_util.io_write_vtu(outname_vtu, V=V_sample)
    # np.savetxt(outname_xyz, V_sample) # ASCII
    # np.save(outname_xyz, V_sample)     # BINARY

# CONVERT MULTIPLE MESHES
def convert_multiple_mesh(filenames):
    for filename in filenames:
        convert_single_mesh(filename)

#
# MAIN SENTINEL
#
if __name__ == '__main__':
    
    # Get the meshes per processor
    meshes = glob.glob(IN_FOLDER+'**/*.off',recursive=True)
    meshes.sort()
    chunksize = math.ceil( len(meshes) / N_PROC )
    per_processor_meshes = [meshes[(i*chunksize):((i+1)*chunksize)] for i in range(N_PROC)]

    # Print some debugging info
    #print(  len(meshes) )
    #print( [len(m) for m in per_processor_meshes ] )
    #print(per_processor_meshes)

    with multiprocessing.Pool(N_PROC) as p:
        p.map(convert_multiple_mesh, per_processor_meshes)
