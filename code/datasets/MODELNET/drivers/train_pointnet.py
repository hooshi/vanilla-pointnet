##
## Train the pointnet model on the MODELNET dataset.
##
## USAGE:
## Starts training pointnet and saves it in models:
## python3 drivers/train_pointnet.py
## Loads already saved model, and keeps training:
## python3 drivers/train_pointnet.py [already-saved-model.torch]
##

import sys 
import os

import matplotlib.pyplot as plt
import numpy as np
import torch

sys.path.append(os.getcwd())
import modelnet_torch
import defs
sys.path.append(defs.TORCH_SRC_PATH)
import torch_predict
import torch_train
import torch_utils



## TORCH DEVICE AND NUMBER OF TRAINING EPOCHS
device = torch.device('cpu')
#device = torch.device('cuda')
NEPOCH=20
# How much of training should be used for validation
# 0.2 for having a lot of validation
# 0 almost no validation -- use for final model
VALIDATION_RATIO=float(sys.argv[2])
PC_SIZE=int(sys.argv[1])

outname="models/pointnet-pcsize%04d-vr%.0f-%s.torch" % (PC_SIZE, VALIDATION_RATIO*100, torch_utils.get_time())

## IF PREVIOUSLY SAVED MODEL+OPTIMIZER IS NOT SPECIFIED, INITIALIZE THE MODEL
if True: #len(sys.argv) <= 1:
    DIMS = [[3, 64, 128, 1024], [1024, 512, 256, 10]]
    LR = 1e-3
    OPTIM='adam'
    BS=50
    POOL='max'

    ctx = modelnet_torch.TrainingContext()
    ctx.init(net_dim0=DIMS[0],
             net_dim1=DIMS[1],
             net_pooling=POOL,
             optim_name=OPTIM,
             batch_size=BS,
             lr = LR,
    )
## OTHERWISE READ THE SAVED MODEL+OPTIMIZER
else:
    ctx = modelnet_torch.TrainingContext()
    ctx.load( sys.argv[1] )
    

## GET TRAINING AND VALIDATION SETS
training_set, validation_set, _ = modelnet_torch.load_dataset(PC_SIZE, VALIDATION_RATIO)

print( "Training set len:", len(training_set) )
print( "Validation set len:",  len(validation_set) )

## TRAIN THE MODEL
loss_vec = []
acc_vec =  []
loss_vec, acc_vec = torch_train.train(
    ctx._net,
    loss_vec,
    acc_vec,
    training_set,
    validation_set,
    device=device,
    print_every=len(training_set)/10,
    eval_every=len(training_set),
    num_epochs=NEPOCH,
    optimizer=ctx._optimizer,
    batch_size=ctx._batch_size
)

## UPDATE THE HISTORY OF ACCURACY AND LOSS
ctx.update_stats( more_epochs=NEPOCH,
                  more_vacc=acc_vec,
                  more_loss=loss_vec
)


## SAVE THE MODEL
print("Dumping to ", outname)
if not os.path.exists('models'):
    os.makedirs('models')
ctx.dump( outname  )

## PLOT LOSS AND VALIDATION ACCURACY
#plt.figure()
#plt.subplot(2,1,1)
#plt.plot(ctx._history_loss)
#plt.subplot(2,1,2)
#plt.plot(ctx._history_vacc)
# plt.show()
