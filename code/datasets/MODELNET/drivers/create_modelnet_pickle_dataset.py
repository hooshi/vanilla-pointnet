#
# Read the model net .vtu pointclouds and convert them to a pickled
# dataset
#

import sys
import os
import glob

import numpy as np

sys.path.append(os.getcwd())
import modelnet_util

VERY_VERBOSE=0
SPACE_DIMS = 3

def generate_globlist(labels, data_path):
    globlist = {}

    for lbl in labels:
        globlist[lbl]=glob.glob(data_path.format(lbl))

    return globlist

def print_globlist(globlist, very_verbose):
    for lbl, addresses in globlist.items():
        print("{}: {}".format(lbl, len(addresses) ))
        if very_verbose:
            for addr in addresses:
                print("{}: {}".format(lbl, addr))

def create_dataset(globlist, pc_size):
    # FIND THE TOTAL NUMBER OF
    n_samples = 0
    for lbl, addresses in globlist.items():
        n_samples += len(addresses)


    # Training data matrices
    XX = np.zeros((n_samples, pc_size, SPACE_DIMS) ,dtype=float)
    yy = np.zeros((n_samples) ,dtype=int)
    vtu_names = np.zeros((n_samples) ,dtype=str)
    yy_as_string = [name for name in globlist.keys()]
    
    # Now read each sample
    sample_id = 0
    for lbl, addresses in globlist.items():        
        for addr in addresses:
            V,_ = modelnet_util.io_read_vtu( addr )
            XX[ sample_id, :, :] = V
            yy[ sample_id ] = yy_as_string.index( lbl )
            vtu_names[ sample_id ] = addr.rsplit('/',1)[-1]
            sample_id += 1
            if sample_id % 200 == 0:
                print("Processed %8d models ..."%sample_id)
            

    return {
        "samples": XX,
        "labels": yy,
        "vtu_names": vtu_names,
        "labels_as_string": yy_as_string,
    }


def main(data_prefix, pc_size):

    train_path = '{}/{{}}/train/*[!.ref].vtu'.format(data_prefix)
    test_path = '{}/{{}}/test/*[!.ref].vtu'.format(data_prefix)

    labels = [
        'toilet',
        'table',
        'sofa',
        'night_stand',
        'monitor',
        'dresser',
        'desk',
        'chair',
        'bed',
        'bathtub'
    ]

    train_globlist = generate_globlist(labels, train_path)
    test_globlist = generate_globlist(labels, test_path)

    print("============= Train")
    print_globlist(train_globlist, VERY_VERBOSE)
    print("============= Test")
    print_globlist(test_globlist, VERY_VERBOSE)
    
    train_dataset = create_dataset(train_globlist, pc_size)
    test_dataset = create_dataset(test_globlist, pc_size)

    modelnet_util.io_write_pickle( train_dataset, 'raw_data/modelnet10-train-pcsize{}.pickle'.format(pc_size) )
    modelnet_util.io_write_pickle( test_dataset, 'raw_data/modelnet10-test-pcsize{}.pickle'.format(pc_size) )

if __name__ == '__main__':
    main(data_prefix='raw_data/ModelNet10_PC4096/',
         pc_size = 4096)
    main(data_prefix='raw_data/ModelNet10_PC2048/',
         pc_size = 2048)
    main(data_prefix='raw_data/ModelNet10_PC1024/',
         pc_size = 1024)
    main(data_prefix='raw_data/ModelNet10_PC0512/',
         pc_size = 512)
    main(data_prefix='raw_data/ModelNet10_PC0256/',
         pc_size = 256)
