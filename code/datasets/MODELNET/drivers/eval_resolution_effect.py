##
## Eval a trained pointnet model on the model net dataset.
##
## USAGE:
## python3 python3 drivers/eval_pointnet.py [saved-model.py]
##

import sys 
import os

import matplotlib.pyplot as plt
import numpy as np
import torch

sys.path.append(os.getcwd())
import modelnet_torch
import defs
sys.path.append(defs.TORCH_SRC_PATH)
import torch_predict
import torch_train
import torch_utils
import torch_metrics

device = torch.device('cpu')

def get_accuracy(model, pc_size, should_plot): 
    
    ## GET MODEL
    ctx = modelnet_torch.TrainingContext()
    ctx.load( model )
    ## GET TRAINING AND VALIDATION SETS
    _, _, test_set = modelnet_torch.load_dataset(pc_size)

    print("Model {}:".format(model))
    ctx.print_info(sys.stdout)
    print( "Test set len:", len(test_set) )
    
    # Get accuracy
    gt_label, pred_proba = torch_predict.predict(ctx._net, test_set, device)
    pred_label = torch_metrics.predict_label( pred_proba )
    acc = torch_metrics.accuracy(gt_label, pred_label)
    print("Test accuracy is: %.4g"%acc)

    ## PLOT LOSS AND VALIDATION ACCURACY
    if should_plot:
        plt.figure()
        plt.subplot(2,1,1)
        plt.plot(ctx._history_loss)
        plt.xticks([])
        plt.ylabel("loss")
        #
        plt.subplot(2,1,2)
        plt.plot(ctx._history_vacc)
        plt.ylabel("validation error")
        plt.xlabel("epoch")
        plt.show()

    return acc


if __name__ == '__main__':
    acc_per_tr_pc_size = {}
    RESOS = [256, 512, 1024, 2048, 4096]
    MODELS = [
        'models/pointnet-pcsize0256-vr0-2019-04-15-0111.torch',
        'models/pointnet-pcsize0512-vr0-2019-04-15-0116.torch',
        'models/pointnet-pcsize1024-vr0-2019-04-15-0124.torch',
        'models/pointnet-pcsize2048-vr0-2019-04-15-0138.torch',
        'models/pointnet-pcsize4096-vr0-2019-04-15-0212.torch',
        ]
    # for itr in range(len(RESOS)):
    #     acc_per_tr_pc_size[RESOS[itr]] =  []
    #     for ite in range(len(RESOS)):
    #         acc = get_accuracy(MODELS[itr], RESOS[ite], False)
    #         acc_per_tr_pc_size[RESOS[itr]].append(acc)

    acc_per_tr_pc_size = {
        256: [0.855, 0.861, 0.861, 0.856, 0.850],
        512: [0.862, 0.877, 0.872, 0.879, 0.878],
        1024: [0.862, 0.882, 0.888, 0.891, 0.888],
        2048: [0.868, 0.873, 0.874, 0.872, 0.871],
        4096: [0.872, 0.884, 0.881, 0.889, 0.888]
    }
    
    print(acc_per_tr_pc_size)

    plt.figure()
    plt.plot(RESOS, acc_per_tr_pc_size[RESOS[0]], '-s' ,label='%d'%RESOS[0])
    plt.plot(RESOS, acc_per_tr_pc_size[RESOS[1]], '-^' ,label='%d'%RESOS[1])
    plt.plot(RESOS, acc_per_tr_pc_size[RESOS[2]], '-v' ,label='%d'%RESOS[2])
    plt.plot(RESOS, acc_per_tr_pc_size[RESOS[3]] , '->',label='%d'%RESOS[3])
    plt.plot(RESOS, acc_per_tr_pc_size[RESOS[4]], '-<',label='%d'%RESOS[4])
    plt.ylabel('test accuracy')
    plt.xlabel('test point cloud size')
    plt.legend()
    plt.savefig('effect.svg', bbox_inches='tight')
    plt.show()
