#
# An example to read the training dataset and view the pointcloud
#

import sys
import os
import glob

import numpy as np

sys.path.append(os.getcwd())
import modelnet_util


# Read train and test
train_data = modelnet_util.io_read_pickle( 'raw_data/modelnet10-train-pcsize1024.pickle' )
test_data =  modelnet_util.io_read_pickle( 'raw_data/modelnet10-test-pcsize1024.pickle' )

# Split training to test and validation
train_ids, validation_ids = modelnet_util.ml_training_and_validation_indices(train_data["labels"].shape[0])
validation_data = modelnet_util.ml_sub_dataset( train_data, validation_ids)
train_data = modelnet_util.ml_sub_dataset( train_data, train_ids)

# View the keys in the data set dictionary
print( "DATA is a dictionary with keys: ", train_data.keys() )
print( "labels as string: ", train_data["labels_as_string"])

# Randomly select a data and save it as vtu to be viewed
sample_id = 123;
X = train_data["samples"]
y = train_data["labels"]
y_as_str = train_data["labels_as_string"]
print( "Training data # {} is a {}={}".format(sample_id, y[sample_id], y_as_str[ y[sample_id] ]) )

outname = 'sample-no-{}.vtu'.format(sample_id)
print("Saving the point cloud in {}".format(outname))
print("Use paraview to to see it")
modelnet_util.io_write_vtu(outname, V=X[sample_id,:,:])
