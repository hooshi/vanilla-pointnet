#
# general definisions that might need changing from computer to computer
#
# Shayan Hoshyari


# Used for reading and writing meshes
MESHIO_PATH = '../..'

# Generic torch code (not tied to dataset)
TORCH_SRC_PATH = '../../torch_src/'

# if you want to read the data later with python 2, change this to 2
PICKLE_PROTOCOL = 3
