#!/usr/bin/env python

import numpy as np

PREAMBLE = r"""\documentclass[11pt]{article}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{xcolor}
\usepackage{soul}
\usepackage[margin=0.3in]{geometry}
\usepackage[active,tightpage]{preview}

\newlength{\tempheight}
\newlength{\tempwidth}
  \newcommand{\rowname}[1]% #1 = text
  {\rotatebox{90}{\makebox[\tempheight][c]{#1}}}
  \newcommand{\columnname}[1]% #1 = text
  {\makebox[\tempwidth][c]{#1}}
"""

class LatexImageWriter:

    def __init__(self,filename):
        self._tx = open(filename, 'w')
        self._env_stack = []
        self.write(PREAMBLE+'\n')
        self.set_img_width(0.99)
        self.write('\\begin{document} \n')
        
    def write(self, what):
        self._tx.write(what)

    def break_line(self):
        self.write('\n')

    def write_col(self, col):
        self.write("\\columnname{{{}}} \n".format(col)) 

    def write_geoms(self, geoms, name=None):

        self.write('\\settoheight{\\tempheight}{\\includegraphics[width=\\tempwidth]{%s}} \n'%geoms[0])
        if name is not None:
            self.write("\\rowname{{{}}} \n".format(name) )
        for g in geoms:
            self.write("\\includegraphics[width=\\tempwidth,height=\\tempheight,keepaspectratio]{{{}}} \n".format(g) )
        self.break_line()

    def set_img_width(self, val):
        assert (val > 0) and (val < 1)
        self.write('\\setlength{\\tempwidth}{%f\linewidth} \n'%val)

    def set_img_height(self, val):
        assert (val > 0) and (val < 1)
        self.write('\\setlength{\\tempheight}{%f\linewidth} \n'%val)

    def push_env(self, env):
        self.write('\\begin{{{}}}\n'.format(env))
        self._env_stack.append(env)
        
    def pop_env(self):
        if not self._env_stack:
            return False
        else:
            self.write('\\end{{{}}}\n'.format(self._env_stack[-1]))
            self._env_stack.pop()
            return True
        
    def destroy(self):
        while(self.pop_env()): pass
        self.write('\\end{document} \n')
        self._tx.close()

##==========================================================
## A small test code
##==========================================================
def test():
    tx = LatexImageWriter('screenshot/test.tex')
    
    tx.push_env('preview')
    tx.push_env('centering')
    tx.set_img_width(0.15)
    # tx.set_img_height(0.15)

    classes = [
    'bathtub',
    'chair',
    'dresser',  
    'night_stand',
    'sofa',   
    'toilet',
    'bed',    
    'desk',
    'monitor',
    'table',
    ]
    
    for class_name in classes:
        images = []
        for i in range(1,6):
            image = '{{{class_name}_{no:04d}}}.png'.format(class_name=class_name, no=i)
            images.append( image )
        tx.write_geoms(images, name=class_name.replace('_',' ') )
    
    tx.destroy()

if __name__ == '__main__':
    test()
