import sys
import os

import matplotlib.pyplot as plt
import numpy as np
import torch

sys.path.append(os.getcwd())
import defs
import modelnet_util
sys.path.append(defs.TORCH_SRC_PATH)
import torch_predict
import torch_train
import torch_utils


## ===========================================================
## Data provider for torch
## ============================================================
class DataProvider(torch.utils.data.Dataset):
    def __init__(self, X, y):
        self.X = X
        self.y = y

    def __len__(self):
        return self.X.shape[0]

    def __getitem__(self, idx):
        ##
        points = self.X[idx, :, :]
        label = self.y[idx]
        ##
        points = torch.FloatTensor(points)
        # torch requries Long for target
        label = torch.from_numpy(np.array([label.astype(np.long)])) 
        return (points, label)

    def is_batchable(self):
        return True

## ===========================================================
## VANILLA POINTNET for the MODELNET DATASET
## ============================================================
class PointNet(torch.nn.Module):
    # dim1: dimension of the MLP before the pooling
    # dim2: dimension of the MLP after the pooling
    # pool_type='mean' or 'max'
    def __init__(self, dim1, dim2, pool_type='mean'):
        super(PointNet, self).__init__()

        assert dim1[-1] == dim2[0] 

        def _dim2mlp(_dim, skip_relu=False):            
            mlp = []
            for i in range(len(_dim)-1):
                mlp.append( torch.nn.Linear(_dim[i], _dim[i+1]) )
                mlp.append( torch.nn.ReLU() )
            if skip_relu :
                mlp.pop()
            return torch.nn.Sequential(*mlp)

        ## Set each operation
        self._mlp1 = _dim2mlp(dim1,False)
        self._mlp2 = _dim2mlp(dim2,True)
        self._pool = pool_type

    # forward pass
    def forward(self, x):
        # single input reshaped to 2-D tensor
        if len(x.shape)==2:
            n_batch = 1
            n_pts = x.shape[0]
            dim = x.shape[1]
        # multiple inputs in a 3-D tensor
        else:
            n_batch = x.shape[0]
            n_pts = x.shape[1]
            dim = x.shape[2]
        ##
        assert dim == 3
        ##
        ## Torch only applies an MLP to 2-D tensor
        x = x.view(-1, dim)
        x = self._mlp1(x)
        ## Move the data back to a 3-D tensor
        x = x.view(n_batch, n_pts, -1)
        ## apply pooling 
        if self._pool == 'max':
            x = torch.max(x, dim=1)[0]
        elif self._pool == 'mean':
            x = torch.mean(x, dim=1)
        ## pass through the last MLP
        x = self._mlp2(x)
        ##
        return x

## ===========================================================
## LOAD THE MODELNET DATA AS TORCH DATASET
## ============================================================
# returns (training_set, validation_set, test_set)
def load_dataset(pc_size=1024, validation_ratio=0.2):
    train_data = modelnet_util.io_read_pickle( 'raw_data/modelnet10-train-pcsize%d.pickle'%pc_size )
    test_data =  modelnet_util.io_read_pickle( 'raw_data/modelnet10-test-pcsize%d.pickle'%pc_size )

    # Split training to test and validation
    train_ids, validation_ids = modelnet_util.ml_training_and_validation_indices(train_data["labels"].shape[0],validation_ratio)
    validation_data = modelnet_util.ml_sub_dataset( train_data, validation_ids)
    train_data = modelnet_util.ml_sub_dataset( train_data, train_ids)
    

    return (
        DataProvider(train_data["samples"], train_data["labels"]),
        DataProvider(validation_data["samples"], validation_data["labels"]),
        DataProvider(test_data["samples"], test_data["labels"]),
    )

## ===========================================================
##  All the data needed for training
## ============================================================
class TrainingContext:
    def __init__(self):
        self._net = None
        self._net_dim0 = None
        self._net_dim1 = None
        self._net_pooling = None
        ##
        self._optimizer = None
        self._optimizer_name = None
        self._batch_size = None
        self._history_vacc = []
        self._history_loss = []
        self._learning_rate = 0
        self._n_total_epoch = 0

    def build_optimizer(self, optim_name, lr):
        if optim_name == 'adam':
            optimizer = torch.optim.Adam(self._net.parameters(), lr=lr)
        elif optim_name == 'sgd':
            optimizer = torch.optim.SGD(self._net.parameters(), lr=lr)
        else:
            assert 0

        return optimizer


    def update_stats(self, more_epochs, more_vacc, more_loss):
        self._n_total_epoch += more_epochs
        self._history_loss.extend( more_loss )
        self._history_vacc.extend( more_vacc )
        

    def dump(self, filename):
        stats = {}
        
        stats[ 'net' ] = self._net.state_dict()
        stats[ 'net_dim0' ] = self._net_dim0
        stats[ 'net_dim1' ] = self._net_dim1
        stats[ 'net_pooling' ] = self._net_pooling
        ##
        stats[ 'optimizer' ] = self._optimizer.state_dict()
        stats[ 'optimizer_name' ] = self._optimizer_name 
        stats[ 'learning_rate' ] = self._learning_rate
        stats[ 'batch_size' ] = self._batch_size
        stats[ 'hist_vacc' ] = self._history_vacc
        stats[ 'hist_loss' ] = self._history_loss
        stats[ 'n_total_epoch' ] = self._n_total_epoch

        torch_utils.save_obj( stats, filename)

    def init(self, net_dim0, net_dim1, net_pooling, optim_name, batch_size, lr):
        self._net_dim0 = net_dim0
        self._net_dim1 = net_dim1
        self._net_pooling = net_pooling
        self._net = PointNet(self._net_dim0, self._net_dim1, self._net_pooling)
        ##
        self._learning_rate = lr
        self._optimizer_name = optim_name
        self._optimizer = self.build_optimizer(self._optimizer_name, self._learning_rate)
        self._batch_size = batch_size
        self._history_vacc = []
        self._history_loss = []
        self._n_total_epoch = 0

        
    def load(self, filename):
        stats = torch_utils.load_obj( filename )

        self._net_dim0 = stats[ 'net_dim0' ]
        self._net_dim1 = stats[ 'net_dim1' ]
        self._net_pooling = stats[ 'net_pooling' ]
        self._net = PointNet(self._net_dim0, self._net_dim1, self._net_pooling)
        self._net.load_state_dict( stats[ 'net' ] )
        ##
        self._optimizer_name = stats[ 'optimizer_name' ]
        self._learning_rate = stats[ 'learning_rate' ]
        self._optimizer = self.build_optimizer(self._optimizer_name, self._learning_rate)
        self._optimizer.load_state_dict(   stats[ 'optimizer' ] )
        self._batch_size = stats[ 'batch_size' ] 
        self._history_vacc = stats[ 'hist_vacc' ]
        self._history_loss = stats[ 'hist_loss' ]
        self._n_total_epoch = stats[ 'n_total_epoch' ] 

    def print_info(self, outstream):
        outstream.write("Point net model : \n")
        outstream.write("Number of units: %s %s\n"%(self._net_dim0,self._net_dim1))
        outstream.write("Pooling type: %s \n"%(self._net_pooling))
        ##
        outstream.write("Training optimizer: %s \n"%(self._optimizer_name))
        outstream.write("Training batch size: %d \n"%(self._batch_size))
        outstream.write("Training learning rate: %e \n"%(self._learning_rate))
        outstream.write("Trained epochs: %d \n"%(self._n_total_epoch))
        

## ===========================================================
##  TEST
## ============================================================
