#
# Functions for the tmnist (traced mnist) data set
#
# Shayan Hoshyari


# Python
import os
import sys
import random
import copy

# more fancy libs
import PIL
import matplotlib.pyplot as plt
import numpy as np 
import pickle

# Local
import defs
import mnist_reader
try:
    import mnist_tracer
except:
    print( "Warning: could not open py_trace, no tracing functionality available" )


# =======================================================
#   dataset.n_samples number of samples in the datasset
#   dataset.labels the labels of each sample
#   dataset.points, dataset.xadj:
#     all the points in the dataset such that the points for
#     sample i are
#     dataset.points[dataset.xadj[i]:dataset.xadj[i+1], :]
# =======================================================
class Data_set:
    def __init__(self, xadj, points, labels):
        self.n_samples = len(xadj)-1
        self.xadj = np.array(xadj, dtype=int)
        self.points = np.array(points, dtype=float)
        self.labels = np.array(labels, dtype=int)
        assert points.shape[0] == self.xadj[-1], "{} != {}".format(points.shape[0], self.xadj[-1])
        assert labels.shape[0] == self.n_samples, "{} != {}".format(labels.shape[0], self.n_samples)

    def copy(self):
        return copy.deepcopy(self)

    def point_ids_for_sample(self, i):
        return np.arange(self.xadj[i], self.xadj[i+1])
    
    def points_for_sample(self, i):
        assert i < self.n_samples
        return self.points[ self.xadj[i]: self.xadj[i+1], :]

    def sub_dataset(self, ids):
        sub_n_samples = len(ids)
        sub_labels = self.labels[ ids ]
        ##
        sub_xadj = np.zeros(sub_n_samples+1, dtype=int)
        for ii in range(sub_n_samples):
            n_points = self.xadj[ ids[ii]+1] - self.xadj[ ids[ii] ]
            sub_xadj[ii+1] = sub_xadj[ii] +  n_points
        ##
        sub_points = np.zeros((sub_xadj[-1],2), dtype=float)
        for ii in range(sub_n_samples):
            sub_points[sub_xadj[ii]:sub_xadj[ii+1], :] =  self.points_for_sample(ids[ii])
        ##
        return Data_set(sub_xadj, sub_points, sub_labels)

            
# =======================================================
# Read the mnist data set. For every image, trace it with
# various points
# Input
#   images is a vector of n_image x 8 x 8 of uint8
#   labels is a vector of n_image of uint8
# Output
#   Returns a struct dataset
#   dataset.n_samples number of samples in the datasset
#   dataset.labels the labels of each sample
#   dataset.points, dataset.xadj:
#     all the points in the dataset such that the points for
#     sample i are
#     dataset.points[dataset.xadj[i]:dataset.xadj[i+1], :]
# =======================================================
def create_dataset(images, labels):
    n_samples = labels.shape[0]
    assert n_samples == images.shape[0] 
    assert images.shape[1] == images.shape[2]

    points = []
    xadj = [0]
    for i in range( n_samples ): 
        traced_image = mnist_tracer.pipeline( images[i] )
        points.extend( list( traced_image.points.flatten() ) )
        xadj.append( xadj[-1] + traced_image.points.shape[0] )
        if( i % 1000 == 0):
            print( "traced {} images ".format(i) )
    assert len(points)/2 == xadj[-1]

    dataset = Data_set(
        labels=np.array(labels),
        xadj=np.array(xadj, dtype=int),
        points=np.array(points, dtype=float).reshape(-1,2)  )
        
    return dataset


# =======================================================
# Is dataset: check if a python object is indeed a dataset
# =======================================================
def is_dataset( dataset ):
    if not hasattr(dataset, 'n_samples'):
        return False, "does not have n_samples"
    if not hasattr(dataset, 'labels'):
        return False, "does not have labels"
    if not hasattr(dataset, 'xadj'):
        return False, "does not have xadj"
    if not hasattr(dataset, 'points'):
        return False, "does not have points"
    return True, ""
    # return isinstance(dataset, Data_set), ""
    # return True, ""

# =======================================================
# Write a traced dataset into a file
# =======================================================
def dump_dataset( dataset, filename ):
    scss, msg =  is_dataset( dataset )
    assert scss, "dataset corrupted: {}".format( msg )
    with open(filename, 'wb') as fl:
        pickle.dump( obj=dataset, file=fl, protocol=defs.PICKLE_PROTOCOL )

# =======================================================
# Read a traced dataset into a file
# =======================================================
def read_dataset( filename ):
    with open(filename, 'rb') as fl:
        dataset = pickle.load( fl )
    scss, msg =  is_dataset( dataset )
    assert scss, "dataset corrupted: {}".format( msg )
    return dataset

# =================================================
# Make the center of mass of the points of a model 0
# dset: dataset to center
# =================================================
def center_dataset( dset ):

    dset2 = dset.copy()
    for isample in range(dset.n_samples):
        pts = dset.points_for_sample(isample)
        center = np.sum(pts, axis=0) / pts.shape[0]
        pts = pts - center
        rr2 = np.amax( np.sum(pts*pts, axis=1) )
        rr = np.sqrt(rr2)
        pts = pts / rr
        dset2.points[ dset.point_ids_for_sample(isample), : ] = pts

    return dset2
    
# =================================================
# Subsample the dataset by removing points that
# are closer than epsilon
# =================================================
def subsample_dataset( dset , eps):

    new_pts = []
    new_xadj = [0]
    ##
    for isample in range(dset.n_samples):
        pts = dset.points_for_sample(isample)
        pts_int = np.round(pts/(10.0*eps)).astype(int)

        _, unique_ids = np.unique(pts_int, return_index=True, axis=0)

        pts_sampled = pts[unique_ids, :]
        new_pts.append(pts_sampled)
        new_xadj.append( new_xadj[-1]+pts_sampled.shape[0] )

    new_pts = np.concatenate(new_pts, axis=0)
    return Data_set(new_xadj, new_pts, dset.labels)

# =================================================
#  Analyzie point size in dataset
# =================================================
def analyze_point_sizes( dset ):

    n_points = dset.xadj[1:]-dset.xadj[:-1]
    max_points = np.max( n_points )
    min_points = np.min( n_points )
    mean_points = np.mean( n_points )
    median_points = np.median( n_points )

    print( max_points )
    print( mean_points )
    print( min_points )
    print( median_points )

    plt.subplot(2,2,1)
    ii = np.argmin( n_points )
    plot_sample(dset, ii)
    #
    plt.subplot(2,2,2)
    ii = np.argmax( n_points )
    plot_sample(dset, ii)

    plt.subplot(2,2,3)
    _img, _lbl = mnist_reader.read("testing","raw_data")
    ii = np.argmin( n_points )
    mnist_tracer.draw_image(_img[ii], plt.gca())
    
    plt.subplot(2,2,4)
    ii = np.argmax( n_points )
    mnist_tracer.draw_image(_img[ii], plt.gca())
    
# =================================================
# Duplicate the last point of every data set
# to make the data sizes equal
# =================================================
def equalize_dataset( dset ):

    n_points = dset.xadj[1:]-dset.xadj[:-1]
    max_points = np.max( n_points )

    new_pts = np.zeros( (max_points*dset.n_samples, 2), dtype=float)
    new_xadj = [0]
    for isample in range(dset.n_samples):
        pts = dset.points_for_sample(isample)
        new_xadj.append( new_xadj[-1]+max_points )
        new_pts[new_xadj[isample]:new_xadj[isample]+pts.shape[0], :] = pts
        new_pts[new_xadj[isample]+pts.shape[0]:new_xadj[isample+1], :] = pts[-1,:]
        
    return Data_set(new_xadj, new_pts, dset.labels)


# =======================================================
# Create the indices for training and validation set
# Assumes that the training dataset is loaded
# uses 10% of the data for validation
# returns (training_ids, validation_ids)
# =======================================================
def training_and_validation_indices(n_samples, validation_ratio=0.2):
    all_ids = np.arange( n_samples )
    ## Shuffle with predictive seed
    rnd = np.random.RandomState(12312332)
    rnd.shuffle( all_ids )
    ##
    n_training_samples = int( (1-validation_ratio) * n_samples )
    training_ids  = all_ids[:n_training_samples]
    validation_ids  = all_ids[n_training_samples:]
    ##
    return (training_ids, validation_ids)
    

# =======================================================
# PLOT ONE DATA SET SAMPLE
# =======================================================
def plot_sample(_dataset, _isample, _pltopt=dict(linestyle='', marker='.')):
    points0 = _dataset.points_for_sample( _isample )
    lbl = _dataset.labels[_isample]
    plt.gca().plot(points0[:,0],points0[:,1], **_pltopt)
    plt.gca().set_ylim(1, 0)
    plt.gca().set_aspect('equal')
    plt.gca().axis('equal')
    plt.gca().set_title("sample {}: {}".format(_isample, lbl))

# =======================================================
# A small test
# =======================================================

def test_tracing():
    (img, lbl) = mnist_reader.read('training', 'raw_data')
    img = img[0:100,:,:]
    lbl = lbl[0:100]
    #
    dataset0 = create_dataset( img, lbl )
    assert is_dataset( dataset0)[0]
    #
    dump_dataset( dataset0, "_test.pickle" )
    #
    dataset1 =  read_dataset(  "_test.pickle" )
    #
    for i in range(9):
        isample = random.randint(0, dataset1.n_samples )
        plt.subplot(3,3,i+1)
        plot_sample(isample)
    #
    plt.show()

def test_loading():
    # dataset1 =  read_dataset(  "raw_data/tmnist_train.pickle" )
    dataset1 =  read_dataset(  "raw_data/tmnist_test_subsample_batch_scale.pickle" )
    print( dataset1.n_samples )
    #
    isamples = []
    for i in range(9):
        isample = random.randint(0, dataset1.n_samples )
        isamples.append( isample )
    ##
    for ii, isample in enumerate(isamples):
        plt.subplot(3,3,ii+1)
        plot_sample(dataset1, isample)
    ##
    plt.figure()
    dataset2 = dataset1.sub_dataset( isamples )
    for ii in range(len(isamples)):
        plt.subplot(3,3,ii+1)
        plot_sample(dataset2, ii)
    
    #
    plt.show()

def test_splitting():
    tr, vl = training_and_validation_indices(20)
    print("Training: ", tr )
    print("Validation: ", vl )

def test_centering():
    dataset1 =  read_dataset(  "raw_data/tmnist_test.pickle" )
    dataset2 = center_dataset( dataset1 )
    
    # dataset2 = copy.copy(dataset1)
    isamples = []
    for i in range(9):
        isample = random.randint(0, dataset1.n_samples )
        isamples.append( isample )
    ##
    for ii, isample in enumerate(isamples):
        plt.subplot(3,3,ii+1)
        plot_sample(dataset1, isample)
        plot_sample(dataset2, isample)
    
    #
    plt.show()

def test_subsmaple():
    dataset1 =  read_dataset(  "raw_data/tmnist_test.pickle" )
    dataset2 = subsample_dataset( dataset1, 0.2 )
    
    # dataset2 = copy.copy(dataset1)
    isamples = []
    for i in range(9):
        isample = random.randint(0, dataset1.n_samples )
        isamples.append( isample )
    ##
    for ii, isample in enumerate(isamples):
        plt.subplot(3,3,ii+1)
        plot_sample(dataset1, isample,dict(linestyle='', marker='.', markersize=1))
        plot_sample(dataset2, isample,dict(linestyle='', marker='o', markersize=2))
    
    #
    plt.show()

def test_equalize():
    dataset1 =  read_dataset(  "raw_data/tmnist_test.pickle" )
    dataset1 = subsample_dataset( dataset1, 0.2 )
    # analyze_point_sizes(dataset1)
    dataset2 = equalize_dataset( dataset1 )
    
    isamples = []
    for i in range(9):
        isample = random.randint(0, dataset1.n_samples )
        isamples.append( isample )
    ##
    for ii, isample in enumerate(isamples):
        plt.subplot(3,3,ii+1)
        plot_sample(dataset1, isample, dict(linestyle='', marker='.', markersize=1))
        plot_sample(dataset2, isample, dict(linestyle='', marker='o', markersize=2))
        # print(dataset2.points_for_sample( isample ) )
    
    #
    plt.show()


if __name__ == '__main__':
    #test_tracing()
    test_loading()
    #test_splitting()
    # test_centering()
    #test_subsmaple()
    #test_equalize()
