import sys
import os

import matplotlib.pyplot as plt
import numpy as np
import torch

sys.path.append(os.getcwd())
import defs
import tmnist
import tmnist_torch
sys.path.append(defs.TORCH_SRC_PATH)
import torch_predict
import torch_train
import torch_utils


## ===========================================================
## TORCH DATA PROVIDER ON MNIST
## ============================================================
class DataProvider(torch.utils.data.Dataset):
    def __init__(self, raw_dataset):
        self.raw_dataset = raw_dataset

    def __len__(self):
        return self.raw_dataset.n_samples

    def __getitem__(self, idx):
        ##
        assert idx < self.raw_dataset.n_samples, "{} < {}".format(idx, self.raw_dataset.n_samples)
        ##
        points = self.raw_dataset.points_for_sample(idx)
        label = self.raw_dataset.labels[idx]
        ##
        points = torch.FloatTensor(points)
        # torch requries Long for target
        label = torch.from_numpy(np.array([label.astype(np.long)])) 
        return (points, label)

    def is_batchable(self):
        sizes = self.raw_dataset.xadj[1:]-self.raw_dataset.xadj[:-1]
        return np.max(sizes) == np.min(sizes)

## ===========================================================
## VANILLA POINTNET for the MNIST DATASET
## ============================================================
class PointNet(torch.nn.Module):
    # dim1: dimension of the MLP before the pooling
    # dim2: dimension of the MLP after the pooling
    # pool_type='mean' or 'max'
    def __init__(self, dim1, dim2, pool_type='mean'):
        super(PointNet, self).__init__()

        assert dim1[-1] == dim2[0] 

        def _dim2mlp(_dim, skip_relu=False):            
            mlp = []
            for i in range(len(_dim)-1):
                mlp.append( torch.nn.Linear(_dim[i], _dim[i+1]) )
                mlp.append( torch.nn.ReLU() )
            if skip_relu :
                mlp.pop()
            return torch.nn.Sequential(*mlp)

        ## Set each operation
        self._mlp1 = _dim2mlp(dim1,False)
        self._mlp2 = _dim2mlp(dim2,True)
        self._pool = pool_type

    # forward pass
    def forward(self, x):
        # single input reshaped to 2-D tensor
        if len(x.shape)==2:
            n_batch = 1
            n_pts = x.shape[0]
            dim = x.shape[1]
        # multiple inputs in a 3-D tensor
        else:
            n_batch = x.shape[0]
            n_pts = x.shape[1]
            dim = x.shape[2]
        ##
        assert dim == 2
        ##
        ## Torch only applies an MLP to 2-D tensor
        x = x.view(-1, 2)
        x = self._mlp1(x)
        ## Move the data back to a 3-D tensor
        x = x.view(n_batch, n_pts, -1)
        ## apply pooling 
        if self._pool == 'max':
            x = torch.max(x, dim=1)[0]
        elif self._pool == 'mean':
            x = torch.mean(x, dim=1)
        ## pass through the last MLP
        x = self._mlp2(x)
        ##
        return x

    # Get the critical points
    # assumes that batch size is 1
    def np_critical_points(self, x_numpy):
        x = torch.FloatTensor( x_numpy )
        x = x.view(-1, 2)
        x = self._mlp1(x)
        pooled_units, critical_ids = torch.max(x, dim=0)
        pooled_units = np.array( pooled_units ) 
        critical_ids = np.unique(  np.array( critical_ids, dtype=int) )
        return  critical_ids,   pooled_units 
    
    # See if a point is critical or upper bound
    def np_upper_bound_points(self, candidates_numpy, pooled_units_numpy):
        candidates = torch.FloatTensor( candidates_numpy )
        candidates = candidates.view(-1, 2)
        candidates = self._mlp1(candidates)

        ## To numpy !!
        candidates = np.asarray( candidates )
        pooled_units = np.repeat(pooled_units_numpy.reshape( 1, -1), candidates.shape[0], axis=0)

        is_less = candidates <= pooled_units 
        sum_is_less = np.sum(is_less, axis=1).astype(int) 
        is_upper_bound = (sum_is_less == candidates.shape[1]).astype(bool)

        #np.savetxt('pooled_units.txt', pooled_units_numpy.reshape( -1, 1))
        #np.savetxt('candidate_0.txt', candidates[0,:].reshape( -1, 1))
        #np.savetxt('isless_0.txt', is_less[0,:].reshape( -1, 1))
        #np.savetxt('sum_isless.txt', sum_is_less)
        #np.savetxt('is_upperbound.txt', is_upper_bound)
        # print(repr(pooled_units_numpy))
        # print( candidates < pooled_units )
        #print( candidates.shape[1] )
        #print( candidates.shape[0] )
        #print( is_upper_bound.shape[0] )
        
        return candidates_numpy [is_upper_bound, :]


## ===========================================================
## LOAD THE MNIST DATA AS TORCH DATASET
## ============================================================
# returns (training_set, validation_set, test_set)
def load_dataset(scaled=True):

    if(scaled):
        base_name = '_subsample_batch_scale'
    else:
        base_name = '_subsample_batch'
        
    train_name = "/raw_data/tmnist_train%s.pickle"%base_name
    test_name = "/raw_data/tmnist_test%s.pickle"%base_name
    print("Loading %s"%train_name)
    print("Loading %s"%test_name)
    tmnist_train = tmnist.read_dataset( defs.MNIST_PATH + train_name )
    tmnist_test = tmnist.read_dataset( defs.MNIST_PATH + test_name )
    
    training_ids, validation_ids = tmnist.training_and_validation_indices( tmnist_train.n_samples, 0.2 )
    training_set = DataProvider( tmnist_train.sub_dataset( training_ids ) )
    validation_set = DataProvider( tmnist_train.sub_dataset( validation_ids ) )
    test_set = DataProvider( tmnist_test )

    return (training_set, validation_set, test_set)

## ===========================================================
##  All the data needed for training
## ============================================================
class TrainingContext:
    def __init__(self):
        self._net = None
        self._net_dim0 = None
        self._net_dim1 = None
        self._net_pooling = None
        ##
        self._optimizer = None
        self._optimizer_name = None
        self._batch_size = None
        self._history_vacc = []
        self._history_loss = []
        self._learning_rate = 0
        self._n_total_epoch = 0

    def build_optimizer(self, optim_name, lr):
        if optim_name == 'adam':
            optimizer = torch.optim.Adam(self._net.parameters(), lr=lr)
        elif optim_name == 'sgd':
            optimizer = torch.optim.SGD(self._net.parameters(), lr=lr)
        else:
            assert 0

        return optimizer


    def update_stats(self, more_epochs, more_vacc, more_loss):
        self._n_total_epoch += more_epochs
        self._history_loss.extend( more_loss )
        self._history_vacc.extend( more_vacc )
        

    def dump(self, filename):
        stats = {}
        
        stats[ 'net' ] = self._net.state_dict()
        stats[ 'net_dim0' ] = self._net_dim0
        stats[ 'net_dim1' ] = self._net_dim1
        stats[ 'net_pooling' ] = self._net_pooling
        ##
        stats[ 'optimizer' ] = self._optimizer.state_dict()
        stats[ 'optimizer_name' ] = self._optimizer_name 
        stats[ 'learning_rate' ] = self._learning_rate
        stats[ 'batch_size' ] = self._batch_size
        stats[ 'hist_vacc' ] = self._history_vacc
        stats[ 'hist_loss' ] = self._history_loss
        stats[ 'n_total_epoch' ] = self._n_total_epoch

        torch_utils.save_obj( stats, filename)

    def init(self, net_dim0, net_dim1, net_pooling, optim_name, batch_size, lr):
        self._net_dim0 = net_dim0
        self._net_dim1 = net_dim1
        self._net_pooling = net_pooling
        self._net = PointNet(self._net_dim0, self._net_dim1, self._net_pooling)
        ##
        self._learning_rate = lr
        self._optimizer_name = optim_name
        self._optimizer = self.build_optimizer(self._optimizer_name, self._learning_rate)
        self._batch_size = batch_size
        self._history_vacc = []
        self._history_loss = []
        self._n_total_epoch = 0

        
    def load(self, filename):
        stats = torch_utils.load_obj( filename )

        self._net_dim0 = stats[ 'net_dim0' ]
        self._net_dim1 = stats[ 'net_dim1' ]
        self._net_pooling = stats[ 'net_pooling' ]
        self._net = PointNet(self._net_dim0, self._net_dim1, self._net_pooling)
        self._net.load_state_dict( stats[ 'net' ] )
        ##
        self._optimizer_name = stats[ 'optimizer_name' ]
        self._learning_rate = stats[ 'learning_rate' ]
        self._optimizer = self.build_optimizer(self._optimizer_name, self._learning_rate)
        self._optimizer.load_state_dict(   stats[ 'optimizer' ] )
        self._batch_size = stats[ 'batch_size' ] 
        self._history_vacc = stats[ 'hist_vacc' ]
        self._history_loss = stats[ 'hist_loss' ]
        self._n_total_epoch = stats[ 'n_total_epoch' ] 

    def print_info(self, outstream):
        outstream.write("Point net model : \n")
        outstream.write("Number of units: %s %s\n"%(self._net_dim0,self._net_dim1))
        outstream.write("Pooling type: %s \n"%(self._net_pooling))
        ##
        outstream.write("Training optimizer: %s \n"%(self._optimizer_name))
        outstream.write("Training batch size: %d \n"%(self._batch_size))
        outstream.write("Training learning rate: %e \n"%(self._learning_rate))
        outstream.write("Trained epochs: %d \n"%(self._n_total_epoch))

    
# =================================================================== 
#   SOME TESTING
# =================================================================== 
def test_data_provider():
    tmnist_data = tmnist.read_dataset( MNIST_PATH + "/raw_data/tmnist_test.pickle" )
    tmnist_data = tmnist_data.sub_dataset( [0,23,1234,152] )
    torch_data = DataProvider( tmnist_data )
    print("n_samples: ", tmnist_data.n_samples )
    print("is_batchable: ", torch_data.is_batchable() )

    tmnist_data = tmnist.subsample_dataset( tmnist_data, 0.2 )
    torch_data = DataProvider( tmnist_data )
    print("is_batchable: ", torch_data.is_batchable() )

    tmnist_data = tmnist.equalize_dataset( tmnist_data )
    torch_data = DataProvider( tmnist_data )
    print("is_batchable: ", torch_data.is_batchable() )

    ##
    print("len torch data :", len(torch_data) )

    ##
    print("BATCH SIZE 1 :")
    torch_loader = torch.utils.data.DataLoader(torch_data, batch_size=1, shuffle=True, num_workers=1)
    for i, data in enumerate(torch_loader):
        samples = data[0]
        labels = data[1]
        print(samples.shape, " ", labels.shape)

    ##
    print("BATCH SIZE 2 :")
    torch_loader = torch.utils.data.DataLoader(torch_data, batch_size=2, shuffle=True, num_workers=1)
    for i, data in enumerate(torch_loader):
        samples = data[0]
        labels = data[1]        
        print(samples.shape, " ", labels.shape)

    ##
    print("BATCH SIZE 3 :")
    torch_loader = torch.utils.data.DataLoader(torch_data, batch_size=3, shuffle=True, num_workers=1)
    for i, data in enumerate(torch_loader):
        samples = data[0]
        labels = data[1]        
        print(samples.shape, " ", labels.shape)



if __name__ == '__main__':
    test_data_provider()
