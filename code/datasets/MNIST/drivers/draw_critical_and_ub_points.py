##
## View the critical and upper bound points for some of the shapes
##
## USAGE:
## python3 python3 drivers/eval_pointnet.py [saved-model.py]
##

import sys 
import os

import matplotlib.pyplot as plt
import numpy as np
import torch

sys.path.append(os.getcwd())
import defs
import tmnist
import tmnist_torch
sys.path.append(defs.TORCH_SRC_PATH)
import torch_predict
import torch_train
import torch_utils
import torch_metrics


device = torch.device('cpu')

if len(sys.argv) <= 2:
    print("Usage python3 drivers/draw_critical_and_ub_points.py [saved-model.py] [is-data-scaled=0 or 1]")
    exit(-1)
else:
    ctx = tmnist_torch.TrainingContext()
    IS_DATA_SCALED = int( sys.argv[2] )
    ctx.load( sys.argv[1] )
    
print("Is data scaled: {}".format(IS_DATA_SCALED))
print("Model {}:".format(sys.argv[1]))
ctx.print_info(sys.stdout)

## GET TRAINING AND VALIDATION SETS
_, _, test_set = tmnist_torch.load_dataset(IS_DATA_SCALED)
print( "Test set len:", len(test_set) )

# Case to analyze
sample_ids = np.array([975])
for i in range(sample_ids.shape[0]):
    sample_id= sample_ids[i]
    plt.figure(0)
    plt.clf()
    ## Get the points
    points, _ = test_set[sample_id]
    points = np.asarray(points)
    ## Get the critical points
    with torch.no_grad(): 
        cr_ids , pooled_units = ctx._net.np_critical_points(points )
    ##
    ## Grid over the domain
    ngrid = 250
    x = np.linspace(-0.7, 0.7, ngrid)
    y = np.linspace(-0.7, 0.7, ngrid)
    x, y = np.meshgrid(x,y)
    candidates = np.concatenate( ( x.reshape(-1,1), y.reshape(-1,1) ), axis=1 )
    #print( candidates )
    with torch.no_grad(): 
        upper_bounds = ctx._net.np_upper_bound_points(candidates, pooled_units)
    ##
    plt.subplot(1,3,1)
    plt.gca().plot(points[:,0],points[:,1], 'o')
    plt.gca().set_ylim(1, 0)
    plt.gca().axis('equal')
    ref_ylim = plt.gca().get_ylim()
    ref_xlim = plt.gca().get_xlim()
    ##
    plt.subplot(1,3,2)
    plt.gca().plot(points[cr_ids,0],points[cr_ids,1], 'o')
    plt.gca().axis('equal')
    plt.gca().set_ylim(*ref_ylim)
    plt.gca().set_xlim(*ref_xlim)    
    ##
    plt.subplot(1,3,3)
    plt.gca().plot(upper_bounds[:,0],upper_bounds[:,1], 'o')
    #plt.gca().plot(candidates [:,0],candidates[:,1], 'o')
    plt.gca().axis('equal')
    plt.gca().set_xlim(*ref_xlim)    
    plt.gca().set_ylim(*ref_ylim)
    break


plt.show()
