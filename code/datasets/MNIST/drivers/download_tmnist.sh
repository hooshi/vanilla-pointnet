#!/bin/bash

#
# Download the TMNIST data set either directly download tmnist.zip from this link
# WITHOUT SCALING
# https://drive.google.com/file/d/1QpETMoLJPJ0AvxyPEovu5h9vuPV9Essb/view?usp=sharing
# WITH SCALING ( NOT MUCH DIFFERENCE , this is the one that is used for the images in the report)
# https://drive.google.com/file/d/1vAj3OpZBv_ZJ5N151t8QKkm_CTlMdAfs/view?usp=sharing
#
# and unzip it under MNIST/raw_data/
#
# Or run
# bash drivers/download_tmnist.sh
#


function download_from_gdrive
{
    FILENAME="$2"
    FILEID="$1"

    FILEID2=$(wget --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=${FILEID}" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')
    
    wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=${FILEID2}&id=${FILEID}" -O "${FILENAME}"

    rm -rf /tmp/cookies.txt
}

# WITHOUT SCALING
# FILEID="1QpETMoLJPJ0AvxyPEovu5h9vuPV9Essb"
# WITH SCALING ( NOT MUCH DIFFERENCE , this is the one that is used for the images in the report)
FILEID="1vAj3OpZBv_ZJ5N151t8QKkm_CTlMdAfs"
mkdir -p raw_data
download_from_gdrive ${FILEID} raw_data/tmnist.zip
unzip raw_data/tmnist.zip
