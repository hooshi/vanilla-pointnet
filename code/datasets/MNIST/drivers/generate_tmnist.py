#
# Generate the tmnist (traced mnist) data set
# run from the parent directory
# (python drivers/generate_tmnist.py)
#
# Shayan Hoshyari

# python
import sys
import os

# Local
sys.path.append(os.getcwd())
import mnist_reader
import tmnist

##
print("Reading mnist training ...")
(img, lbl) = mnist_reader.read('training', 'raw_data')
print("Tracing ...")
dataset = tmnist.create_dataset( img, lbl )
print("Dumping to raw_data/tmnist_train.pickle ...")
tmnist.dump_dataset( dataset, "raw_data/tmnist_train.pickle" )

## 
print("Reading mnist testing ...")
(img, lbl) = mnist_reader.read('testing', 'raw_data')
print("Tracing ...")
dataset = tmnist.create_dataset( img, lbl )
print("Dumping to raw_data/tmnist_test.pickle ...")
tmnist.dump_dataset( dataset, "raw_data/tmnist_test.pickle" )
