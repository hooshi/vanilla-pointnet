##
## Train the pointnet model on the tmnist dataset.
##
## USAGE:
## Starts training pointnet and saves it in models:
## python3 drivers/train_pointnet.py
## Loads already saved model, and keeps training:
## python3 drivers/train_pointnet.py [already-saved-model.torch]
##

import sys 
import os

import matplotlib.pyplot as plt
import numpy as np
import torch

sys.path.append(os.getcwd())
import defs
import tmnist
import tmnist_torch
sys.path.append(defs.TORCH_SRC_PATH)
import torch_predict
import torch_train
import torch_utils



## TORCH DEVICE AND NUMBER OF TRAINING EPOCHS
#device = torch.device('cuda')
device = torch.device('cpu')
NEPOCH=20
IS_DATA_SCALED = True

## IF PREVIOUSLY SAVED MODEL+OPTIMIZER IS NOT SPECIFIED, INITIALIZE THE MODEL
if len(sys.argv) <= 1:
    DIMS = [[2, 64, 128, 1024], [1024, 512, 256, 10]]
    LR = 1e-3
    OPTIM='adam'
    BS=300
    POOL='max'

    ctx = tmnist_torch.TrainingContext()
    ctx.init(net_dim0=DIMS[0],
             net_dim1=DIMS[1],
             net_pooling=POOL,
             optim_name=OPTIM,
             batch_size=BS,
             lr = LR,
    )
## OTHERWISE READ THE SAVED MODEL+OPTIMIZER
else:
    ctx = tmnist_torch.TrainingContext()
    ctx.load( sys.argv[1] )
    

## GET TRAINING AND VALIDATION SETS
training_set, validation_set, _ = tmnist_torch.load_dataset(IS_DATA_SCALED)

print( "Training set len:", len(training_set) )
print( "Validation set len:",  len(validation_set) )

## TRAIN THE MODEL
loss_vec = []
acc_vec =  []
loss_vec, acc_vec = torch_train.train(
    ctx._net,
    loss_vec,
    acc_vec,
    training_set,
    validation_set,
    device=device,
    print_every=len(training_set)/10,
    eval_every=len(training_set),
    num_epochs=NEPOCH,
    optimizer=ctx._optimizer,
    batch_size=ctx._batch_size
)

## UPDATE THE HISTORY OF ACCURACY AND LOSS
ctx.update_stats( more_epochs=NEPOCH,
                  more_vacc=acc_vec,
                  more_loss=loss_vec
)


## SAVE THE MODEL
outname="models/pointnet-{}.torch".format(torch_utils.get_time())
print("Dumping to ", outname)
if not os.path.exists('models'):
    os.makedirs('models')
ctx.dump( outname  )

## PLOT LOSS AND VALIDATION ACCURACY
plt.figure()
plt.subplot(2,1,1)
plt.semilogy(ctx._history_loss)
plt.subplot(2,1,2)
plt.plot(ctx._history_vacc)
plt.show()
