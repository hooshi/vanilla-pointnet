#
# Some conditioning on the tmnist data set to make it easier for learning
#
# 1- Center all pointclouds
#
# 2- Downsample all pointclouds (two points closer than 0.02 would be merged)
#
# 3- Make number of points in each sample equal, so that the samples
# can be batched in pytorch. Otherwise, it is not easy to have batch
# size of more than one wiith the pytorch interface.
#
# Reads from  raw_data/tmnist_[train,test].pickle
# Writes to  raw_data/tmnist_[train,test]_subsample_batch.pickle
#
# Shayan Hoshyari

# python
import sys
import os

# Local
sys.path.append(os.getcwd())
import mnist_reader
import tmnist

##
print("tmnist training ...")
tmnist_dataset = tmnist.read_dataset( "raw_data/tmnist_train.pickle" )
tmnist_dataset = tmnist.subsample_dataset(tmnist_dataset, 0.2)
tmnist_dataset = tmnist.equalize_dataset(tmnist_dataset)
tmnist_dataset = tmnist.center_dataset(tmnist_dataset)
tmnist.dump_dataset( tmnist_dataset, "raw_data/tmnist_train_subsample_batch_scale.pickle" )

## 
print("tmnist testing ...")
tmnist_dataset = tmnist.read_dataset( "raw_data/tmnist_test.pickle" )
tmnist_dataset = tmnist.subsample_dataset(tmnist_dataset, 0.2)
tmnist_dataset = tmnist.equalize_dataset(tmnist_dataset)
tmnist_dataset = tmnist.center_dataset(tmnist_dataset)
tmnist.dump_dataset( tmnist_dataset, "raw_data/tmnist_test_subsample_batch_scale.pickle" )
