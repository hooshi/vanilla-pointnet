#!/bin/bash

mkdir -p raw_data
wget http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz -O raw_data/train-images-idx3-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz -O raw_data/train-labels-idx1-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz  -O raw_data/t10k-images-idx3-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz  -O raw_data/t10k-labels-idx1-ubyte.gz
gunzip raw_data/*.gz
