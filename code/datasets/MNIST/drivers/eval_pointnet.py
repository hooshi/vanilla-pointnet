##
## Eval a trained pointnet model on the tmnist dataset.
##
## USAGE:
## python3 python3 drivers/eval_pointnet.py [saved-model.py]
##

import sys 
import os
import random

import matplotlib.pyplot as plt
import numpy as np
import torch

sys.path.append(os.getcwd())
import defs
import tmnist
import tmnist_torch
sys.path.append(defs.TORCH_SRC_PATH)
import torch_predict
import torch_train
import torch_utils
import torch_metrics


device = torch.device('cpu')
DRAW_WRONG_CASES = False
DRAW_CORRECT_CASES = False

if len(sys.argv) <= 2:
    print("Usage python3 drivers/eval_pointnet.py [saved-model.py] [is-data-scaled=0 or 1]")
    exit(-1)
else:
    ctx = tmnist_torch.TrainingContext()
    IS_DATA_SCALED = int( sys.argv[2] )
    ctx.load( sys.argv[1] )
    
print("Is data scaled: {}".format(IS_DATA_SCALED))
print("Model {}:".format(sys.argv[1]))
ctx.print_info(sys.stdout)

## GET TRAINING AND VALIDATION SETS
_, _, test_set = tmnist_torch.load_dataset(IS_DATA_SCALED)
print( "Test set len:", len(test_set) )

# Get ground truth and predicted labels
gt_label, pred_proba = torch_predict.predict(ctx._net, test_set, device)
pred_proba = np.exp(pred_proba) / np.sum( np.exp(pred_proba) , axis=1).reshape(-1,1) 
pred_label = torch_metrics.predict_label( pred_proba )

# Get accuracy
accuracy = torch_metrics.accuracy(gt_label, pred_label)
print("Test accuracy is: %.4g"%accuracy)

# Get confusion matrix
conf = torch_metrics.confusion_matrix(gt_label, pred_label)
print("Confusion")
print (conf)

# Get precision and recall
precision, recall = torch_metrics.precision_recall(conf)
print("Precision - Recall")
print(np.concatenate((precision.reshape(-1,1), recall.reshape(-1,1)), axis=1) )

# Fully plot a model
def plot_model(model_id, outname, show_ub=False):            
    points, _ = test_set[model_id]
    points = points.numpy()
    ## Plot the case
    plt.figure(0)
    plt.clf()
    plt.gca().plot(points[:,0],points[:,1], 'o')
    plt.gca().set_ylim(1, 0)
    plt.gca().set_aspect('equal')
    plt.gca().axis('equal')
    ref_xlim = plt.gca().get_xlim()
    ref_ylim = plt.gca().get_ylim()
    ## Set the probabilities
    plt.title('{}, {}'.format(gt_label[model_id],
                              np.array2string(pred_proba[model_id,:], precision=2, separator=',', suppress_small=True) ))
    plt.gca().set_xticks([])
    plt.gca().set_yticks([])
    ##
    plt.savefig('sample_plots/{}-0.pdf'.format(outname), bbox_inches='tight')
    if not show_ub: return
    
    ##
    ## Plot the critical points and upper bounds
    ## Get the critical points
    with torch.no_grad(): 
        cr_ids , pooled_units = ctx._net.np_critical_points(points )
    ##
    ## Grid over the domain
    ngrid = 250
    x = np.linspace(-0.9, 0.9, ngrid)
    y = np.linspace(-0.9, 0.9, ngrid)
    x, y = np.meshgrid(x,y)
    candidates = np.concatenate( ( x.reshape(-1,1), y.reshape(-1,1) ), axis=1 )
    with torch.no_grad(): 
        upper_bounds = ctx._net.np_upper_bound_points(candidates, pooled_units)
    ##
    plt.clf()
    plt.gca().plot(points[cr_ids,0],points[cr_ids,1], 'o')
    plt.gca().axis('equal')
    plt.gca().set_ylim(*ref_ylim)
    plt.gca().set_xlim(*ref_xlim)    
    plt.gca().set_xticks([])
    plt.gca().set_yticks([])
    plt.savefig('sample_plots/{}-1.pdf'.format(outname), bbox_inches='tight')
    ##
    plt.clf()
    plt.gca().plot(upper_bounds[:,0],upper_bounds[:,1], 'o')
    plt.gca().axis('equal')
    plt.gca().set_xlim(*ref_xlim)    
    plt.gca().set_ylim(*ref_ylim)
    plt.gca().set_xticks([])
    plt.gca().set_yticks([])
    plt.savefig('sample_plots/{}-2.pdf'.format(outname), bbox_inches='tight')


# Plot all the wrong examples
if DRAW_WRONG_CASES:
    wrong_ids =  np.arange(gt_label.shape[0])[ (gt_label != pred_label) ]
    for i in range(wrong_ids.shape[0]):
        wrong_id= wrong_ids[i]
        plot_model(wrong_id, "wrong-{}".format(i), False)

if DRAW_CORRECT_CASES:
    correct_ids =  np.arange(gt_label.shape[0])[ (gt_label == pred_label) ]
    random.shuffle( correct_ids )
    for i in range(40):
        _id= correct_ids[i]
        plot_model(_id, "correct-{}".format(i), True)


## PLOT LOSS AND VALIDATION ACCURACY
plt.figure(figsize=(4,2)) 
plt.subplot(2,1,1)
plt.semilogy(ctx._history_loss)
plt.xticks([])
plt.ylabel("loss")
#
plt.subplot(2,1,2)
plt.plot(ctx._history_vacc)
plt.ylabel("validation error")
plt.xlabel("epoch")
plt.savefig('sample_plots/tmnist-training.pdf', bbox_inches='tight')
plt.show()
