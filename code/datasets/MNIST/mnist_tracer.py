#
# trace mnist digits using potrace
#
# Shayan Hoshyari

# Python
import os
import sys

# more fancy libs
import PIL
import matplotlib.pyplot as plt
import numpy as np

# Local
import defs
import mnist_reader
try:
    sys.path.append(defs.PY_TRACE_PATH); import py_trace
except:
    raise RuntimeError( "Warning: could not open py_trace" )

# ===============================================================
#  Draw an image from the mnist dataset 
# ===============================================================
def draw_image(image, ax=None):
    """
    Render a given numpy.uint8 2D array of pixel data.
    """
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
    imgplot = ax.imshow(image, cmap=plt.cm.Greys)
    imgplot.set_interpolation('nearest')
    # ax.xaxis.set_ticks_position('top')
    # ax.yaxis.set_ticks_position('left')
    ax.set_yticks([])
    ax.set_xticks([])
    ax.axis('off')

# ===============================================================
#  Draw a traced image
#  tr is a struct returned by the  trace_image() function
# ===============================================================
def draw_traced(tr, ax=None):
    """
    Render a traced image
    """
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
    for i in range( tr.n_segment ):
        current_points =  tr.points[tr.xadj[i]:tr.xadj[i+1],:]
        x = np.concatenate( ( current_points[:,0], [current_points[0,0]] ) )
        y = np.concatenate( ( current_points[:,1], [current_points[0,1]] ) )
        ax.plot(x,y, 'b.', markersize=2)
    ax.axis('off')
    ax.set_ylim(1, 0)
    ax.set_aspect('equal')
    ax.axis('equal')
    ax.set_yticks([])
    ax.set_xticks([])

    
# ===============================================================
#  Scale an image using bicubic interpolation
#  image should be a numpy array
# ===============================================================
def scale_image(image, scale):
    pil_image = PIL.Image.fromarray( image )
    width, height = pil_image.size
    pil_image = pil_image.resize((width*scale, height*scale) , resample=PIL.Image.BICUBIC)
    return np.array( pil_image )
    

# ===============================================================
#  Threshold_Image a BW image to make it binary colored
#  image should be a numpy int array
#  val should be an int between 0 and 255
# ===============================================================
def threshold_image(image, val):
    black = image < val
    image[ black ] = 0
    image[ ~black ] = 255
    return image

# ===============================================================
#  Trace an image using potrace
#  - image should be a numpy int array
#  - returns a struct holding
#      n_segment: number of curves in the traced image
#      sign:      array of len n_segment, this can be ignored
#                 sign[i] == int('-') if the segment should be filled
#                 with white, and sign[i] == int('+') if the segment
#                 should be filled with black
#      xadj:       numpy int array of length n_segment+1
#      points:     numpy number_of_points x 2 array
#      The points belonging to segment i are points[ xadj[i]: xadj[i+1], :]
# ===============================================================
def trace_image( image, verbose ):
    # Create the input dictionary
    inp = {}
    image_int = [int(pix) for pix in image.flatten()]
    inp['pixels'] = image_int
    inp['height'] = image.shape[0] 
    inp['width'] = image.shape[1] 
    inp['opticurve'] = 1
    inp['alphamax'] = 3.
    inp['turdsize'] = 2
    inp['verbose'] = verbose
    # ans_dict is a dictionary
    # ans_dict['n_segment'] is an int
    # ans_dict['sign'] is an array of int with size n_segment
    # ans_dict['xadj'] is an array of  int with size n_segment+1
    # ans_dict['points'] is an array of double with size 2*xadj(wn_segment)
    ans_dict = py_trace.py_trace( inp )
    #
    ans = lambda:0
    ans.n_segment = ans_dict['n_segment']
    ans.sign = ans_dict['sign']
    ans.xadj = np.array(ans_dict['xadj'], dtype=int)
    ans.points = np.array(ans_dict['points'],dtype=float)
    ans.points = ans.points.reshape(-1,2)
    #
    return ans

# =======================================================
# pipeline,
# runs an image through the  whole tracing pipeline
# I.e, scale, threshold_image, trace
# =======================================================
def pipeline(image, scale=2, thr_val=125, verbose=False):
    image = scale_image(image, scale)
    image = threshold_image( image, thr_val )
    tr = trace_image( image, verbose )
    return tr

# =======================================================
# A simple test
# grab an image from the mnist data set, scale it to deal
# with antialiasing, and then trace it.
# =======================================================
def test():
    fig = plt.figure(figsize=(16,4))
    ax0 = fig.add_subplot(1,4,1)
    ax1 = fig.add_subplot(1,4,2)
    ax2 = fig.add_subplot(1,4,3)
    ax3 = fig.add_subplot(1,4,4)

    # grab the image and draw it
    (img, lbl) = mnist_reader.read('training', 'raw_data')
    im = img[1, :, :]
    draw_image( im, ax0 )

    # scale the image and draw it
    im = scale_image(im, 2)
    draw_image( im, ax1 )
    
    # threshold the image and draw it
    im = threshold_image( im, 125 )
    draw_image( im, ax2 )

    # trace the image and draw it
    tr = trace_image( im, verbose=True )
    draw_traced( tr, ax3 )

    plt.savefig('out.svg', bbox_inches='tight')
    plt.show()

    


if __name__ == '__main__':
    test()

