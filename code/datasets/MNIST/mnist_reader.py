#
# Read mnist dataset
#
# Shayan Hoshyari

import os
import struct
import numpy as np

from matplotlib import pyplot
import matplotlib as mpl

"""
https://gist.github.com/akesling/5358964
Loosely inspired by http://abel.ee.ucla.edu/cvxopt/_downloads/mnist.py
which is GPL licensed.
"""

def read(dataset = "training", path = "."):
    """
    Python function for importing the MNIST data set.  Returns a tuple
    where the first index is the images, and the second one is the
    lables.
    """
    if dataset == "training":
        fname_img = os.path.join(path, 'train-images-idx3-ubyte')
        fname_lbl = os.path.join(path, 'train-labels-idx1-ubyte')
    elif dataset == "testing":
        fname_img = os.path.join(path, 't10k-images-idx3-ubyte')
        fname_lbl = os.path.join(path, 't10k-labels-idx1-ubyte')
    else:
        assert 0, "dataset must be 'testing' or 'training' not '{}' ".format( dataset ) 

    # Load everything in some numpy arrays
    with open(fname_lbl, 'rb') as flbl:
        magic, num = struct.unpack(">II", flbl.read(8))
        lbl = np.fromfile(flbl, dtype=np.int8)

    with open(fname_img, 'rb') as fimg:
        magic, num, rows, cols = struct.unpack(">IIII", fimg.read(16))
        img = np.fromfile(fimg, dtype=np.uint8).reshape(len(lbl), rows, cols)


    # Create an iterator which returns each image in turn
    #get_img = lambda idx: (lbl[idx], img[idx])
    #for i in xrange(len(lbl)):
    #    yield get_img(i)

    return (img, lbl)

