#
# general definisions that might need changing from computer to computer
#
# Shayan Hoshyari

import os

# This should point to the location of the py_potrace cython library
PY_TRACE_PATH='../../potrace/build-opt/pypotrace/'

TORCH_SRC_PATH = '../../torch_src/'

MNIST_PATH = os.getcwd()

# if you want to read the data later with python 2, change this to 2
PICKLE_PROTOCOL = 3
