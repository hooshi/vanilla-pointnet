\section{Experiments}
\label{sec:exp}

We have implemented the vanilla pointnet architecture using the PyTorch library~\cite{paszke2017automatic}, and validated it on the traced MNIST and ModelNet10~\cite{wu20153d} datasets. The traced MNIST dataset was created as part of this work (see Section~\ref{sec:tmnist} for details) as a variant of its predecessor, MNIST~\cite{deng2012mnist}, by shifting the data representation from pixels to point clouds.

% ======================================================
%        TRACED MNIST
% ======================================================
\subsection{Traced MNIST Dataset}
\label{sec:tmnist}

\textbf{Dataset Overview:}  The traced MNIST dataset consists of 48k training samples, 12k validation samples, and 10k test samples. Each sample contains a point cloud that represents ``the outline'' (see Fig. \ref{fig:tmnist-creation}d) of a hand-written digit. We have created these point clouds by processing the original images in the MNIST datasets, following the Potrace~\cite{selinger2012potrace} bitmap tracing pipeline (see Figure \ref{fig:tmnist-creation}). Each digit image is thresholded to black and white and then passed to the Potrace library so that its outline is traced as smooth Bezier curves. Then, points are sampled (with a distance of 0.1 pixel) from the Bezier curves to construct the sample point clouds. To construct smoother outlines and leverage the information hidden in the anti-aliased pixels, we first scale the MNIST images(2x) with linear interpolation before thresholding them. To automate this process, we wrote a Python binding for the Potrace C library, and then processed the whole MNIST dataset via a Python script.
%
\begin{figure}
\centering
  \includegraphics[width=0.55\linewidth]{img/tmnist-creation}
    \caption{\textbf{Creating the traced MNIST dataset}: (a) Sample from the MNIST dataset, (b) scaling by linear interpolation, (c) thresholding, (d) tracing with the Potrace library~\cite{selinger2012potrace}}
  \label{fig:tmnist-creation}
\end{figure}

\textbf{Training and Evaluation:} For the first FC network in the vanilla PointNet architecture (Figure \ref{fig:our_PointNet}) we have used layer sizes of $2$, $64$, $128$, and $1024$ for the input, hidden, and output units, respectively. The second FC network layer sizes are $1024$, $512$, $256$, and $10$ for the input, hidden, and output units, respectively. Note that the number of units in the first network ($2$) and the number of output units in the second network ($10$) are dictated by the space dimensions and the number of classes, respectively. The number of layers and the number of hidden units have the same values as the original work of Qi et al.~\cite{qi2017PointNet}. We experimented with using smaller numbers of layers or hidden units, but could not acheive competing results. For training, we used the Adam optimizer~\cite{kingma2014adam} with a learning rate of $10^{-3}$ and a batch size of $300$ for $70$ effective passes over data (epochs). The training loss and validation accuracy are shown in Figure~\ref{fig:tmnist-training}.
%
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/tmnist-training}
  \caption{\textbf{Loss and validation accuracy over epochs for training on the Traced MNIST dataset}}
  \label{fig:tmnist-training}
\end{figure}
%
We evaluated our model on the test set and achieved a test accuracy of $96.5\%$ (cf. the accuracy of $99\%$ achieved by the methods that use inputs represented by pixels~\cite{deng2012mnist}). The accuracies and precisions per class are shown in Table \ref{tab:tmnist-precision}. The table shows that our model achieves above $90\%$ both in accuracy and precision across all categories in the Traced MNIST digit classification task.
%
\begin{table}
\centering
\caption{\textbf{Precision and recall for each class in Traced MNIST test set}}
\label{tab:tmnist-precision}
\begin{tabular}{c ccc ccc ccc c}
    \hline
    category &0 &1 &2 &3 &4 &5 &6 &7 &8 &9  \\
    \hline 
    precision &98.9 &99.2 &96.0 &93.0 &97.7 &98.7 &98.8 &97.1 &91.4 &94.9 \\
    recall    &97.2 &98.6 &96.5 &97.1 &97.1 &93.3 &96.7 &95.8 &97.5 &94.7 \\
    \hline
 \end{tabular}
\end{table}
 

\textbf{Qualitative Analysis}: Figure \ref{fig:tmnist-sucess} provides some successful predictions from our model. Each example is provided with an input (along with its ground truth label and softmax probability over the categories from $0$ to $9$).
%
% Examples of correctly classified cases
\begin{figure}
  \centering
  \includegraphics[width=0.98\linewidth]{img/tmnist-success}
    \caption{\textbf{Examples of correctly classified cases in Traced MNIST test set}. The ground truth label and the predicted probabilities over the categories from $0$ to $9$ of the classifier are shown above the input.}
  \label{fig:tmnist-sucess}
\end{figure}
%
On the other hand, Figure \ref{fig:tmnist-failure} shows the failed predictions. The failure cases are separated to the left and right regions. The left region contains examples that are difficult for a human to classify; the right region contains the ones that are easier for a human to classify.
% Examples of failure cases
\begin{figure}
  \centering
  \includegraphics[width=0.98\linewidth]{img/tmnist-failure}
  \caption{\textbf{Examples of wrongly classified cases in Traced MNIST test set}. The ground truth label and the predicted probabilities ranging from the category $0$ to the category $9$ are shown above the input. On the right we showcase which are trivial for a human to classify, whereas, on the left, we show more ambiguous cases.}
  \label{fig:tmnist-failure}
\end{figure}


Additionally, following the visualization procedure of Qi et al. \cite{qi2017PointNet}, we studied what the model has learned by visualizing the critical and the upper bound point-sets of an input shape (see Figure \ref{fig:tmnist-sucess}). The critical points are the set of points that contribute to the max pooling layer. In other words, it reflects a small set of points that dictate the output of the network. The upper bound point-set is the set of all points in space whose addition to the corresponding sample will not change the values in the max pooled units. Intuitively, one would expect the upper-bound points to be the interior of the model, as adding points to the interior of the shape should not change its class. Figure~\ref{fig:tmnist-sucess} validates that this is indeed the case.


% ======================================================
%        MODEL NET
% ======================================================
\subsection{ModelNet10 Dataset}

\textbf{Dataset Overview:} To further examine our implemented network, we evaluated it on the ModelNet10~\cite{wu20153d} dataset. The dataset contains ten categories of  3D CAD models of common real-life objects. Figure \ref{fig:modelnet_examples} shows the 10 categories along with some examples of the training set.
%
\begin{figure}
  \centering
  \includegraphics[width=1.0\linewidth]{img/modelnet-examples}
  \caption{\textbf{Examples of samples in the ModelNet10 training data}}
  \label{fig:modelnet_examples}
\end{figure}
%
ModelNet10 contains surface triangle meshes. To sample point clouds from the meshes (see Figure \ref{fig:modelnet_pointcloud_size}) we incorporate the following procedure. First, we sample a triangle from the mesh where the sampling  probability of each triangle corresponds to its area. Once a triangle is chosen, we uniformly select a point from the the triangle. By repeating the procedure $n$ times (triangles are allowed to be picked more than once) we obtain a point-cloud with $n$ points.
%
\begin{figure}
  \centering
  \includegraphics[width=0.7\linewidth]{img/modelnet-pointcloud-sampling}
  \caption{\textbf{Point cloud sampling}: input mesh, and point-clouds sampled with $256, 512, 1024, 2048$, and $4096$ points}
  \label{fig:modelnet_point cloud}
\end{figure}

\textbf{Training and Evaluation:} Here,  we have once again chosen the hyper-parameters (i.e., number of layers, hidden unit sizes, and point-cloud size) based on the the work of Qi et al.~\cite{qi2017PointNet}. We use the same model (except for changing the input unit size to $3$ in accordance to the space dimensions) and training procedure as in the previous experiment. Also, we use point cloud sizes of $1024$. Although the model can handle arbitrary sizes, a fixed size amongst all training samples is necessary for training in batches.  We obtained an average test accuracy of $88.6\%$ (cf. the accuracy of $89.2\%$ for the full PointNet architecture on the bigger dataset of ModelNet50~\cite{qi2017PointNet}). Using $10\%$ of the training data as our validation set and a batch size of $50$, the training loss and validation accuracies over epochs are provided in Figure \ref{fig:modelnet_loss}. 
%
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/modelnet10-training}
  \caption{\textbf{Loss and validation accuracy over epochs for training on the ModelNet10 dataset}. The model was both trained and validated on 1024-sized point clouds.}
  \label{fig:modelnet_loss}
\end{figure}
%
The precision and recall over each category are shown in Table \ref{tab:modelnet-precision}. Not surprisingly, classes with distinct shapes (e.g., toilets, sofas, and monitors) are identified with high precision and recall values, whereas the classifier has struggled to distinguish  classes with similar shapes (e.g., desks and tables).
%
\begin{table}
\centering
\caption{\textbf{Precision and recall for each class in ModelNet10 test set.} The model was both trained and tested on 1024-sized point clouds.}
\label{tab:modelnet-precision}
\scalebox{0.9}{
\begin{tabular}{c ccc ccc ccc c}
    \hline
    category &toilet &table &sofa &night stand &monitor &dresser &desk &chair &bed &bathtub  \\
    \hline 
    precision &97.2 &85.7 &98.0 &84.3 &95.1 &70.3 &72.4 &99.0 &100.0  &84.2 \\
    recall    &100.0 &78.0 &97.0 &68.6 &98.0 &90.7 &73.3 &100.0 &86.0 &96.0 \\
    \hline
\end{tabular}}
\end{table}


One practical question is the effect of point cloud size on accuracy. We conducted a study where we trained and tested our model on various point cloud sizes. Figure \ref{fig:modelnet_pointcloud_size} shows the test accuracy obtained via different combinations of point cloud sizes. It seems that the model performs equally well for point cloud sizes of $1024$ and $4096$. Strangely, the accuracy drops for the model trained on a point cloud size of $2048$. We suspect that this might be due to an artifact in our training procedure, where we have selected the model parameters at the last iterate of the Adam optimizer, rather than selecting the model paramters that result in the smallest loss value. Not surprisingly, smaller point cloud sizes of $256$ and $512$ perform relatively poorly. This study can explain why Qi et al.~\cite{qi2017PointNet} have selected the size of $1024$ for their sampled point clouds, as it offers the best trade-off between speed and accuracy.
%
\begin{figure}
  \centering
  \includegraphics[width=0.6\linewidth]{img/pcsize-effect}
  \caption{\textbf{Effect of various point cloud sizes at training and testing}}
  \label{fig:modelnet_pointcloud_size}
\end{figure}



%% Emacs latex configurations; please keep at the end of the file.
%% Local Variables:
%% TeX-master: "../implementing-vanilla-pointnet"
%% End: 
