\documentclass[11pt]{article}

\usepackage{url}
\usepackage{verbatim}
\usepackage{nicefrac}
% control the float environment
\usepackage{float}
% include graphics
\usepackage{graphicx}
% set margins
\usepackage[margin=1in]{geometry}
% don't indent for a new paragraph, skip line.
\usepackage{parskip}
% math fonts
\usepackage{amsmath}
\usepackage{amssymb}
 % pseudo-code
\usepackage{algorithm2e}
% Bibliography
\usepackage[square]{natbib}
\usepackage{bibentry}
% colors
\usepackage{color}
\usepackage[usenames,dvipsnames]{xcolor}
% colorful links
\usepackage[allbordercolors={0.8 0.8 0.8}]{hyperref}
% NICER FONT
\usepackage[T1]{fontenc}
\usepackage{kpfonts}%  for math    
\usepackage{libertine}%  serif and sans serif
\usepackage[scaled=0.85]{beramono}%% mono


% MACROS
\newcommand{\R}{\mathbb{R}}
\newcommand{\reffig}[1]{{Figure~\ref{#1}}}
\newcommand{\reftab}[1]{{Table~\ref{#1}}}

%% ===================================
%% DOCUMENT BEGIN
%% ===================================
\begin{document}
\title{A Short Survey of Deep Learning Methods for 3-D Geometric Models\vspace{-0.5cm}}
\author{Shayan Hoshyari, Zicong Fan, Dylan Green\vspace{-2cm}}
\date{}

\maketitle

% =======================================
% THE REVIEW
% =======================================

%
% The review must include
%
% A description of the overall topic, and the key themes/trends across
% the papers.
%
% A short high-level description of what was explored in each paper.
% For example, describe the problem being addressed, the key components
% of the proposed solution, and how it was evaluated.  Besides, it
% is important to comment on the why questions: why is this problem
% important and why would this particular solution method make progress
% on it?  It’s also useful to comment on the strengths and weaknesses
% of the various works, and it’s particularly helpful if you can show how
% some works address the weaknesses of prior works (or introduce new
% weaknesses).
%
% One or more logical “groupings” of the papers.  This could be in
% terms of the variant of the topic that they address, in terms of the
% solution techniques used, or in chronological terms.
%

% =======================================
% Description of the overall topic
% =======================================
\section{Deep Learning for 3-D Geometry}

Three-dimensional geometric models are widely used in many fields such as computer graphics, computer-aided design (CAD), visualization, and multimedia.
%
These models are represented in various forms, the most common of which include surface meshes, point clouds, voxels, and implicit functions (shown in \reffig{fig:geometry-represenations}).
%
Naturally, algorithms for processing such data are of high demand. Following the great success of deep learning methods for image processing, there has been a recent trend to generalize such methods to 3-D data. \reffig{fig:deep-learning-applications} shows some of the applications where deep learning methods have been most successful.
%
In this document, we will review some of the recent papers that devise deep learning architectures to reason about 3-D geometric models.

\begin{figure}
  \centering
 \includegraphics[width=0.8\linewidth]{img/geom-representation}
  \caption{Examples of 3-D geometry representations: a) surface mesh, b) point cloud,   c) voxels, and d) implicit function. Note that implicit functions are often, but not necessarily, stored on a spatial grid. Furthermore, voxels are a special case of piecewise constant binary implicit functions that are true in the grid cells that lie inside the shape and are false otherwise.}
  \label{fig:geometry-represenations}
\end{figure}

\begin{figure}
  \centering
 \includegraphics[width=0.8\linewidth]{img/applications}
  \caption{Examples of deep learning applications on 3-D data: a) classification, b) semantic segmentation, c) closest object retrieval, and d) point-cloud normal estimation. Note that in the retrieval task considered here is not concerned with \emph{generating} similar examples, instead the goal is to \emph{find} similar examples in the training data using a metric defined by the networks hidden units. Furthermore, the point cloud normal estimation task is trivial unless the pointcloud is relatively sparse.}
  \label{fig:deep-learning-applications}
\end{figure}

% =======================================
% 
% =======================================
\subsection{Multi-View Based Methods}

% TODO: clean up

We start with the work of \citet{su2015multi}, which is based on a simple observation: a 3-D model can be rendered as one or multiple 2-D images, which can, in turn, be processed by conventional convolutional neural networks (CNN). This approach is often referred to as multi-view based and can be used for applications such as object classification and model retrieval. The networks are referred to as a multi-view convolutional neural network (MVCNN).

\reffig{fig:mvcnn} depicts a schematic of the MVCNN workflow. First, the input which is 3-D surface mesh is rendered as 2-D images from various angles. These images are fed to a conventional CNN (tied across all renders). The per-image outputs of the first CNN are then pooled (using the max function) to produce a single set of hidden units and are then passed to a second CNN. The second CNN will then output per-class probabilities, or its hidden units at a particular layer can be used to define a metric for object retrieval.

\begin{figure}
  \centering
  \includegraphics[width=0.80\linewidth]{img/mvcnn}
  \caption{A schematic of the MVCNN architecture. Image is courtesy of \citet{su2015multi}.}
  \label{fig:mvcnn}
\end{figure}

The method is evaluated on the ModelNet40 \citep{wu20153d} dataset for classification and object retrieval tasks. It achieves an accuracy of $90\%$ for classification,  which has not been beaten (at least by a considerable margin) even to this date (\reftab{tab:accuracy-comparison} shows comparisons against other methods).
%
A strength of this method is that it leverages existing tools in the image processing community, and it can use pre-trained networks on natural images. 
%
On the other hand, the method has many parameters such as the location and angle of the camera for the rendered images, and the parameters of two large CNNs for image classification.
%
Furthermore, it is not clear how the method can be extended for applications that need per point labels (e.g., segmentation or normal estimation).

% =======================================
% 
% =======================================
\subsection{Voxel Based Methods}

Another obvious extension of CNNs to 3-D data is to represent geometric models as a field over a spatial grid, where convolution operations are well defined, and optimized implementations are available. Common choices include a binary inside-outside function, the surface normal field, and distance from the surface (possibly multiplied by a Gaussian filter).


In this front, \citet{wu20153d} use a binary function on a uniform $30\times30\times30$ grid as their input data representation. Instead of a discriminative model, they build a generative, convolutional deep belief network (CDBN). This allows them to support a wide range of applications such as classification, simultaneous shape recognition and completion from depth images, next best view prediction, object retrieval, and even object generation. The paper introduces the ModelNet dataset and uses it to validate the performance of the network. The main weaknesses of the method are that training a generative model is quite difficult and requires a lot of heuristic machinery. Moreover, an input resolution of $30\times30\times30$ (a restriction imposed by memory and computation cost)  might not be enough to capture the inputs well enough, which can partly explain the loss of accuracy in classification (\reftab{tab:accuracy-comparison}). 

% Talk about OCNN and OctNet.
\citet{Wang-2017-OCNN} and \citet{riegler2017octnet} observe that the change in the input field is only abrupt near the model surface, and use it to alleviate the high memory cost of a uniform spatial grid. Instead of a uniform grid, they use an octree (one among many approaches to build a non-uniform grid) to represent the input field and implement pooling and convolution operations for this representation. The main difference between their methods is different algorithmic choices for efficient octree representation and convolution operations on the GPU. Also, \citet{Wang-2017-OCNN} use the normal field as the input, while \citet{riegler2017octnet} use the binary inside-outside function. The methods are evaluated on the ModelNet \citep{wu20153d}, \citep{riemenschneider2014learning}, and \citep{SHREC16-topology} datasets for applications including classification, segmentation, and retrieval (see \reftab{tab:accuracy-comparison} for comparisons of classification accuracy). Perhaps the main downside of this two papers is that they cannot be trivially implemented for execution on the GPU using conventional neural network libraries (e.g., TensorFlow~\citep{abadi2016tensorflow}, PyTorch~\citep{paszke2017automatic}) and need hand-tailored GPU code. Moreover, the approach of \citet{Wang-2017-OCNN} requires the surface normals, which might not be readily available, or cannot be confidently computed for complicated sparse point clouds (and it obviously cannot do normal estimation). Finally, in some cases, it is not clear which improvements are due to the novel convolution operations, and which are due to the different network architecture, or training procedures.

% TODO: if there was time add an image for grid vs. octree

% =======================================
% 
% =======================================
\subsection{Departing From Voxels}

Several works have explored the alternative to voxel-based convolutions. \citet{li2016fpnn} propose a field probing scheme (\reffig{fig:fpnn}). They first convert the input shape into a distance field filtered by a Gaussian to increase the attention to the model surface. They would then sample this field at several positions via a set of probes. Note that each probe is simply a set of positions and weights which act as shown in \reffig{fig:fpnn}. The positions and weights of each filter are learned during training, while the number of filters and the number of points/weights per filter are hyper-parameters. The output units of the probes are then passed to a fully connected network to predict per-class probabilities. The method only supports the task of classification, and is validated on the ModelNet40~\citep{wu20153d} dataset (see \reftab{tab:accuracy-comparison} for accuracy). The proposed method also has many hyper-parameters (number of probes and their positions and weights) and is very sensitive to initialization. On the other hand, the method is much more memory efficient than uniform grid based convolutions, while it is much simpler to implement than octree based convolutions.

\begin{figure}
  \centering
  \includegraphics[width=0.98\linewidth]{img/fpnn}
  \caption{A schematic of the field probing neural network (FPNN) architecture for classification. The input is a geometric model (a), the distance from surface field of the model, $f$, is calculated (b), the distance is multiplied by a Gaussian filter to increase the attention to the model surface (c), each probe which consists of a set of positions, $x_i$, and weights, $w_i$ (d) outputs a hidden unit $z=\sum_i w_if(x_i)$ (e). The hidden units $z$ are then passed through a fully connected layer (f) to obtain the final per-class probabilities (g).}
  \label{fig:fpnn}
\end{figure}

\citet{qi2017pointnet} introduce a  network architecture that can directly process point clouds. The input to the network is the 3-D position (and optionally additional information such as normals or colors) of a variable number of points, $n$. The main challenge, in comparison to a conventional convolutional neural network (CNN), is that the output should be invariant under rigid body transformations or permutations of the input points. They introduce the vanilla pointnet architecture (shown in \reffig{fig:pointnet}) which satisfies the order invariance property and theoretically prove that it can approximate any order invariant function, given enough hidden units. With the addition of further heuristics, they attempt to improve the networks invariance under rigid body transformation and allow for part segmentations. We do not go into the detail of these heuristics as they were beaten by the authors' next extension of pointnet~\citep{qi2017pointnet++}. The method is validated by considering tasks such as object recognition, segmentation of single objects, and segmentation of scenes on multiple 3-D shape datasets (ModelNet40~\citep{wu20153d}, Shapenet Part~\citep{shapenet2015}, and Stanford 3D semantic parsing~\citep{2017arXiv170201105A} datasets). The proposed network architecture achieves results on par with the state of the art methods that use convolutions on voxels (see \reftab{tab:accuracy-comparison} for classification accuracy). Since sampling points from the surface of a 3-D model leads to much less data compared to multi-view renders or uniform volumetric sampling. Thus, the proposed network is more efficient ($\sim 10-100\times$) and has fewer parameters ($\sim 10\times$) compared to its grid-based and multi-view-based predecessors. The sampling of point clouds can also be done adaptively for further optimization. Nevertheless, the neighborhood of each point is completely ignored in the network, until the very last max-pooling layer. As this strategy is ignoring valuable information from the neighborhood (e.g., curvature), the proposed network might need a substantial number of training examples and can be very sensitive. The authors' next work~\citep{qi2017pointnet++} attempts to fix this issue.


\begin{figure}
  \centering
  \includegraphics[width=0.80\linewidth]{img/pointnet-classification}
  \caption{A schematic of the vanilla pointnet architecture for classification. The input is a $n\times 3$ matrix, where each row is the coordinates of a point in the point-cloud. First, each point is passed through a fully connected neural network with $(3,64,128,1024)$ units tied across all points. The per-point outputs are then pooled using the max function, and passed to a second fully-connected neural network with $(1024, 512, 256, k)$ units, where $k$ is the number of classes.}
  \label{fig:pointnet}
\end{figure}

% =======================================
% 
% =======================================
\subsection{Generalized Convolution and Pooling Operations}

Some papers introduce novel convolution and pooling like the transformation that are not based on voxel grids. \citet{klokov2017escape} introduce such an operation based on $k$-d trees. Their framework for classification is shown in \reffig{fig:klokov}. Given an input point cloud, a $k$-d tree of a certain depth is constructed based on the point cloud's density. The point cloud is then subsampled such that a single point exists per tree leaf which constitutes the input of the network.
%
For each tree level and each stump direction, a matrix, $W$, and a bias vector, $b$, is defined. 
%
Then, at each layer of the network, the depth of the tree is reduced by one, and the hidden unit of each new leaf is found as a function of its children, and the $W$ matrix and $b$ vector of the corresponding stump. For example, in \reffig{fig:klokov},
\[ z_4 = \sigma\left( W_\text{green} \begin{bmatrix}   z_8 \\ z_9 \end{bmatrix} + b_\text{green} \right), \]
where $\sigma$ is the activation function. The $W$ matrices and $b$ vectors are the parameters of the network and are determined by training.
%
The approach is validated on the MNIST, ShapeNet Core~\citep{shapenet2015}, and  ModelNet~\citep{wu20153d} datasets for classification and part segmentation (see \reftab{tab:accuracy-comparison} for classification accuracy). Overall, the method seems to be very accurate in classification compared the concurrent work. For segmentation, the method seems to be inferior to pointnet~\citep{qi2017pointnet}. It would be interesting to see the performance of the method on more complicated datasets, and more challenging tasks such as scene segmentation. Also, the $k$-d tree construction task does not seem to be very GPU friendly and constitutes a large portion of the training/testing time.
 
\begin{figure}
  \centering
  \includegraphics[width=0.98\linewidth]{img/klokov}
  \caption{A schematic of the classification architecture of \citet{klokov2017escape}. a) Input shape as a point cloud. b) A $k$-d tree is constructed based on the density of this point-cloud. c) The point-cloud is subsampled, such that there is only a single point per leaf. These points are then transformed to arrive at a single output vector. d) The order at which the input and hidden unit points are combined is shown in more detail. Circles with the same colors denote parameter tying. The figure is partly adapted from \citep{klokov2017escape}.}
  \label{fig:klokov}
\end{figure}

Similarly, \citet{qi2017pointnet++} introduce a novel convolution and pooling like transformation. Instead of a $k$-d tree, they use the vanilla pointnet~\citep{qi2017pointnet} network to build this operation. A simplified version of their network for classification is shown in \reffig{fig:pointnet++}. Given an input point cloud, they sample a series of furthest points and then apply tied pointnet architectures to all the points in a certain neighborhood of the sampled ones. They also introduce an approach to combine multiple neighborhood sizes of each sample, to increase the network's robustness to nonuniform point cloud densities. The process is repeated for multiple layers until a single final unit (but with a large dimension) is arrived at. This unit is finally passed to a fully connected neural network that generates per-class probabilities. A slightly more complicated hourglass network with skip connections is also proposed for shape segmentation, where interpolation is used as the deconvolution operator. The proposed network is validated on the MNIST, ModelNet40~\citep{wu20153d}, SHREC16~\citep{SHREC16-topology}, and ScanNet~\citep{dai2017scannet} datasets for classification and segmentation (see \reftab{tab:accuracy-comparison} for classification accuracy). The method outperforms ablations studies are also performed to investigate the effect of missing data and non-uniform point cloud densities, which the method successfully handles. Pointnet++ seems to be a strong architecture, at the cost of many parameters, and non-GPU friendly operations (e.g., furthest point sampling, and finding points within a certain distance).

% figure for pointnet++
\begin{figure}
  \centering
  \includegraphics[width=0.98\linewidth]{img/pointnetplusplus}
  \caption{A schematic of the classification architecture of pointnet++~\citep{qi2017pointnet++}. a) Input shape as a point cloud (in blue). b) A series of farthest distance points are sampled from the point cloud (in red). c) A tied vanilla pointnet network~\citep{qi2017pointnet} is applied at a certain neighborhood of each point to arrive at the next layers hidden units. d) This process is repeated until we arrive at a single unit with a possibly large dimension. e) The final unit is passed through a fully connected neural network that outputs per-class probabilities.}
  \label{fig:pointnet++}
\end{figure}

\citet{atzmon2018point} define convolution and pooling operations on pointclouds by leveraging radial basis functions (RBF). Their convolution operation is shown in \reffig{fig:atzmon}.
%
This operation first extends per-point information in a point-cloud to a continuous function in the ambient volume using RBF basis functions. Then, a volume convolution is applied to this smooth function, where the convolution itself is also constructed using RBFs. Finally, the volumetric function is restricted back to the point cloud by sampling.
%
In a similar manner, pooling and restriction (see original paper for more details) are defined and used to construct classification and segmentation networks.
%
The networks are validated on the ModelNet40~\citep{wu20153d} and Shapenet Part~\citep{shapenet2015} datasets for classification and segmentation, respectively (see \reftab{tab:accuracy-comparison} for classification accuracy).
%
One plus side of this approach is perhaps its elegance and unifying nature. Also, it is theoretically justified to be robust to noisy and sparse point-clouds. On the other hand, the extension/restriction operation heavily rely on sparse matrix operations and are difficult to optimize.

% figure for atzmon
\begin{figure}
  \centering
  \includegraphics[width=0.98\linewidth]{img/atzmon} \\
  \makebox[0.24\linewidth][c]{(a)} \hfil
  \makebox[0.24\linewidth][c]{(b)} \hfil
  \makebox[0.24\linewidth][c]{(c)} \hfil 
  \makebox[0.24\linewidth][c]{(d)} \\
  \caption{Schematic of the convolution operation of \citet{atzmon2018point}. A function defined on a point-cloud (a) is first extended to the ambient volume using RBF basis functions (b), to which a volume convolution is then applied (c). Finally, the volumetric function is restricted back to the point cloud by sampling (d). The  Figure is courtesy of \citet{atzmon2018point}.}
  \label{fig:atzmon}
\end{figure}

% =======================================
%  COMPARING CLASSIFICATION ACCURACY
% =======================================
\begin{table}
  \centering
  \caption{Classification accuracy of the reviewed methods on the ModelNet40 dataset.}
  \label{tab:accuracy-comparison}
  \begin{tabular}{ll| ll}
    \hline
    method &accuracy &method &accuracy \\
    \hline
    MVCNN$^+$ \citep{su2015multi}   &$90.1\%$
    &3-D Shapenets \citep{wu20153d} &$77.3\%$ \\
    OCNN$^*$ \citep{Wang-2017-OCNN} &$90.6\%$ 
    &OCTNET\citep{riegler2017octnet}   &$86.5\%$ \\
    FPNN \citep{li2016fpnn} &$88.4\%$
    &pointnet \citep{qi2017pointnet} &$89.2\%$\\
    escape-from-cells \citep{klokov2017escape}   &$90.6\%$
    &pointnet++ \citep{qi2017pointnet++} &$90.7\%$ \\
    RBF based convolution \citep{atzmon2018point} &$92.3\%$
    && \\
    \hline
  \end{tabular}
  \raggedright
  $^+$: uses mesh structure \\
  $^*$: uses normals \\  
\end{table}

% =======================================
% 
% =======================================
\subsection{Graph Based Methods}

As mentioned above, pointnet achieved promising results by applying feed-forward operations on individual points and aggregating them with an unordered aggregating operation. Since pointnet does not sufficiently exploit local structure, pointnet++ was introduced to address this issue by partitioning points into smaller clusters and perform pointnet operations on each cluster. As a result, pointnet++ provides an improvement in performance but suffers from computing time (also a slight increase in the number of parameters compared to vanilla pointnet). Due to the trending attention of graph-based neural network in the deep learning community \citep{kipf2016semi, yang2018graph, teney2017graph} (semi-supervised document classification, image scene graph generation and visual question answering), there is also attention for exploiting graph structure for point clouds. For example, KCNet was introduced by \citet{shen2018mining} to improve the computational efficiency of pointnet++. Specifically, they provide two operations for local point cloud structure. One operation is to define a convolution kernel for point cloud to capture the local geometric structure of point cloud.  Another emphasizes on feature aggregation on a k-nearest-neighbor graph based on 3d position. As a result, KCNet achieves comparable accuracy in point cloud classification and parts segmentation tasks while maintaining the number of parameters and inference speed similar to that of the vanilla pointnet (see \reftab{tab:kcnet}).


\begin{table}
\centering
\caption{Model size and inference time. ``M'' stands for million. Networks were tested on a PC with a single NVIDIA GTX 1080 GPU and an Intel i7-8700@3.2 GHz 12 cores CPU (caption provided in the original paper). }
\label{tab:kcnet}
\begin{tabular}{ccc}
\hline
Method             & \#params (M) & Fwd. time (ms) \\ 
\hline

  PointNet (vanilla) & \textbf{0.8} & \textbf{11.6}  \\ 
PointNet++         & 1.0          & \textbf{163.2} \\ 
KCNet (M=16)       & 0.9          & 18.5           \\ 
KCNet (M=3)        & 0.9          & 12.0           \\
\hline
\end{tabular}
\end{table}

% =======================================
% 
% =======================================
\newpage
\bibliographystyle{unsrtnat} % Full names, sort by alphabetical order
\bibliography{geometric-learning-survey}


% =======================================
% LIST OF PAPERS
% Comment this out in the final version
% =======================================
\newpage
\appendix
% We must write a survey of these. According to the guidelines here \url{https://www.cs.ubc.ca/~schmidtm/Courses/540-W19/a5.pdf}.

\section{Explicit List of Reviewed Papers}

\nobibliography*

\begin{itemize}
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\item Methods based on multi-view images
  \begin{itemize}
  \item[(1)] \bibentry{su2015multi}
  \end{itemize}
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\item Methods based on grids
  \begin{itemize}
  \item[(2)] \bibentry{wu20153d}
  \item[(3)] \bibentry{riegler2017octnet}
  \item[(4)] \bibentry{Wang-2017-OCNN}
  \end{itemize}
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\item New ideas for pointclouds
  \begin{itemize}
  \item[(5)] \bibentry{li2016fpnn}
  \item[(6)] \bibentry{qi2017pointnet}
  \item[(7)] \bibentry{qi2017pointnet++}
  \item[(8)] \bibentry{klokov2017escape}
  \item[(9)] \bibentry{atzmon2018point}
  \end{itemize}
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\item Graph neural network for point clouds
  \begin{itemize}
  \item[(10)] \bibentry{shen2018mining}
  \end{itemize}
\end{itemize}

\end{document}

%% Emacs latex configurations; please keep at the end of the file.
%% Local Variables:
%% TeX-master: "geometric-learning-survey"
%% End: 

